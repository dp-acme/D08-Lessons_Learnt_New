package services;

import java.util.Collection;
import java.util.Date;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import utilities.AbstractTest;
import domain.TagKey;
import domain.TagValue;
import domain.Trip;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations={"classpath:spring/datasource.xml","classpath:spring/config/packages.xml"})
@Transactional
public class TagValueServiceTest extends AbstractTest{

	@Autowired 
	private TagValueService tagValueService;

	@Autowired 
	private TagKeyService tagKeyService;

	@Autowired 
	private TripService tripService;
	
	//***CRUD TEST*****
	
	@Test
	public void testFindAll(){
		Collection<TagValue> result;
		
		result = tagValueService.findAll();
		
		Assert.notNull(result);
	}
	
	@Test
	public void testFindOne(){
		List<TagValue> all;
		TagValue tagValueFromList, tagValueFromRepository;
		
		all = (List<TagValue>) tagValueService.findAll();
		Assert.notEmpty(all);
		tagValueFromList = all.get(0);
		
		tagValueFromRepository = tagValueService.findOne(tagValueFromList.getId());
		
		Assert.notNull(tagValueFromRepository);
		Assert.isTrue(tagValueFromList.equals(tagValueFromRepository));
	}
	
	@Test
	public void testSave(){
		TagValue tagValue;
		List<TagKey> tagKeys;
		TagKey tagKey;
		List<Trip> trips;
		Trip trip;
		
		trips = (List<Trip>) tripService.getTripsPublishedAfter(new Date());
		Assert.notEmpty(trips);
		trip = trips.get(0);
		
		tagKeys = (List<TagKey>) tagKeyService.findAll();
		Assert.notEmpty(tagKeys);
		tagKey = tagKeys.get(0);
		
		this.authenticate(trip.getManager().getUserAccount().getUsername());

		tagValue = tagValueService.create();
		tagValue.setValue("Valor de prueba");
		tagValue.setTagKey(tagKey);
		
		tagValue = tagValueService.save(tagValue);
		
		trip.getTags().add(tagValue);
		trip = tripService.save(trip);
		
		this.unauthenticate();

		Assert.isTrue(tagValueService.findAll().contains(tagValue));
		Assert.isTrue(trip.getTags().contains(tagValue));
	}
	
	@Test
	public void testDelete(){
		TagValue tagValue;
		List<TagKey> tagKeys;
		TagKey tagKey;
		List<Trip> trips;
		Trip trip;
		
		trips = (List<Trip>) tripService.getTripsPublishedAfter(new Date());
		Assert.notEmpty(trips);
		trip = trips.get(0);
		
		tagKeys = (List<TagKey>) tagKeyService.findAll();
		Assert.notEmpty(tagKeys);
		tagKey = tagKeys.get(0);

		this.authenticate(trip.getManager().getUserAccount().getUsername());
		
		tagValue = tagValueService.create();
		tagValue.setValue("Valor de prueba");
		tagValue.setTagKey(tagKey);
		
		tagValue = tagValueService.save(tagValue);
		
		trip.getTags().add(tagValue);
		trip = tripService.save(trip);

		Assert.isTrue(tagValueService.findAll().contains(tagValue));
		Assert.isTrue(trip.getTags().contains(tagValue));

		tagValueService.delete(tagValue);
		
		Assert.isTrue(!tagValueService.findAll().contains(tagValue));
		Assert.isTrue(!trip.getTags().contains(tagValue));
		
		this.unauthenticate();
	}
}
