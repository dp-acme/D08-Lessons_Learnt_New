
package services;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import security.LoginService;
import utilities.AbstractTest;
import domain.Actor;
import domain.Folder;
import domain.FolderType;
import domain.Message;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
	"classpath:spring/datasource.xml", "classpath:spring/config/packages.xml"
})
@Transactional
public class FolderServiceTest extends AbstractTest {

	@Autowired
	private FolderService	folderService;

	@Autowired
	private ActorService	actorService;


	@Test
	public void findAllFoldersTest() {
		List<Folder> folders;

		folders = (List<Folder>) this.folderService.findAll();

		Assert.notNull(folders);
	}

	@Test
	public void findFolderTest() {
		List<Folder> folders;
		Folder folder;

		folders = (List<Folder>) this.folderService.findAll();
		Assert.notNull(folders);
		folder = folders.get(0);

		this.authenticate(folder.getActor().getUserAccount().getUsername());

		Assert.isTrue(this.folderService.findAll().contains(this.folderService.findOne(folder.getId())));

		this.authenticate(null);
	}

	@Test
	public void createFolderTest() {
		this.authenticate("manager1");

		Actor actor;
		Folder folder, result;
		int sizeOld, sizeNew;

		actor = this.actorService.findByUserAccountId(LoginService.getPrincipal().getId());

		sizeOld = actor.getFolders().size();

		folder = this.folderService.create(actor);
		folder.setName("TestFolder");
		result = this.folderService.save(folder);

		sizeNew = this.actorService.findByUserAccountId(LoginService.getPrincipal().getId()).getFolders().size();

		Assert.notNull(this.folderService.findOne(result.getId()));

		Assert.isTrue(sizeOld + 1 == sizeNew);
	}

	@Test
	public void saveFolderTest() {
		this.authenticate("manager1");

		Actor actor;
		Folder folder, result;
		String nameOld, nameNew;

		actor = this.actorService.findByUserAccountId(LoginService.getPrincipal().getId());

		folder = this.folderService.create(actor);
		folder.setName("new asdTestFolder");
		result = this.folderService.save(folder);

		folder = result;
		nameOld = folder.getName();

		folder.setName("new fdgTestName");
		result = this.folderService.save(folder);
		nameNew = result.getName();

		Assert.notNull(this.folderService.findOne(result.getId()));
		Assert.isTrue(!nameOld.equals(nameNew));
	}

	@Test
	public void deleteTest() {
		int sizeOld, sizeNew;
		Folder folder, result;
		Actor actor;

		this.authenticate("ranger1");

		actor = this.actorService.findByUserAccountId(LoginService.getPrincipal().getId());

		folder = this.folderService.create(actor);
		folder.setName("TestFolder");
		result = this.folderService.save(folder);

		sizeOld = actor.getFolders().size();
		folder = this.folderService.findOne(result.getId());

		this.folderService.delete(folder);

		sizeNew = this.actorService.findByUserAccountId(LoginService.getPrincipal().getId()).getFolders().size();

		Assert.isTrue(sizeOld - 1 == sizeNew);

		this.authenticate(null);
	}

	@Test(expected = java.lang.IllegalArgumentException.class)
	public void deleteTestSystemFolder() {
		Collection<Folder> foldersOld, foldersNew;
		Folder folder;
		Actor actor;

		this.authenticate("ranger1");

		actor = this.actorService.findByUserAccountId(LoginService.getPrincipal().getId());

		foldersOld = actor.getFolders();
		folder = this.folderService.getFolderByActorIdAndType(actor.getId(), FolderType.INBOX);

		this.folderService.delete(folder);

		foldersNew = this.actorService.findByUserAccountId(LoginService.getPrincipal().getId()).getFolders();

		Assert.isTrue(foldersOld.size() != foldersNew.size());

		this.authenticate(null);
	}

	@Test(expected = java.lang.IllegalArgumentException.class)
	public void deleteTestNoAuth() {
		int sizeOld, sizeNew;
		Folder folder, result;
		Actor actor;

		this.authenticate("ranger1");

		actor = this.actorService.findByUserAccountId(LoginService.getPrincipal().getId());

		folder = this.folderService.create(actor);
		folder.setName("TestFolder");
		result = this.folderService.save(folder);

		sizeOld = actor.getFolders().size();
		folder = this.folderService.findOne(result.getId());

		this.authenticate(null);
		this.authenticate("ranger2");

		this.folderService.delete(folder);

		sizeNew = this.actorService.findByUserAccountId(LoginService.getPrincipal().getId()).getFolders().size();

		Assert.isTrue(sizeOld - 1 == sizeNew);

		this.authenticate(null);
	}

	@Test
	public void getFolderByActorIdAndTypeTest() {
		Folder folder;
		Actor actor;
		List<Actor> actors;

		actors = (List<Actor>) this.actorService.findAll();
		actor = actors.get(0);

		folder = null;
		for (final Folder f : actor.getFolders())
			if (f.getType().getFolderType().equals(FolderType.SPAMBOX)) {
				folder = f;
				break;
			}

		Assert.notNull(folder);
		Assert.isTrue(folder == this.folderService.getFolderByActorIdAndType(actor.getId(), FolderType.SPAMBOX));
	}

	@Test
	public void getFolderByActorAndMessageTest() {
		Folder folder;
		Message message;
		Actor actor;
		List<Actor> actors;
		List<Folder> folders;
		List<Message> messages;

		actors = (List<Actor>) this.actorService.findAll();
		actor = actors.get(0);
		folders = (List<Folder>) actor.getFolders();
		folder = folders.get(0);
		messages = (List<Message>) folder.getMessages();
		message = messages.get(0);

		Assert.notNull(message);
		Assert.notNull(folder);
		Assert.isTrue(folder == this.folderService.getFolderByActorAndMessage(actor.getId(), message.getId()));
	}

	@Test
	public void getAllNotificationFoldersTest() {
		Folder folder;
		Actor actor;
		List<Actor> actors;

		actors = (List<Actor>) this.actorService.findAll();
		actor = actors.get(0);
		folder = this.folderService.getFolderByActorIdAndType(actor.getId(), FolderType.NOTIFICATIONBOX);

		Assert.notNull(folder);
		Assert.isTrue(this.folderService.getAllNotificationFolders().contains(folder));
	}

	@Test
	public void changeFolderTest() {
		this.authenticate("manager1");

		Actor actor;
		Folder folder, result;
		String nameOld, nameNew;

		actor = this.actorService.findByUserAccountId(LoginService.getPrincipal().getId());

		folder = this.folderService.create(actor);
		folder.setName("TestFolder");
		result = this.folderService.save(folder);

		folder = result;
		nameOld = folder.getName();

		folder.setName("testNameFolder");
		result = this.folderService.save(folder);
		nameNew = result.getName();

		Assert.notNull(this.folderService.findOne(result.getId()));
		Assert.isTrue(!nameOld.equals(nameNew));

		this.unauthenticate();
	}

	@Test
	public void getUserParentFoldersTest() {
		Actor actor;
		Collection<Folder> folders;
		Collection<Folder> userFolders;
		Folder folder;

		folders = this.folderService.findAll();
		folder = null;
		actor = null;

		for (final Folder f : folders)
			if (f.getType().getFolderType().equals(FolderType.USERBOX)) {
				actor = f.getActor();
				folder = f;
				break;
			}

		Assert.notNull(folder);
		Assert.notNull(actor);

		userFolders = this.folderService.getUserParentFolders(actor.getId());

		Assert.isTrue(userFolders.contains(folder));
	}

	@Test
	public void getUserFoldersTest() {
		Actor actor;
		Collection<Folder> folders;
		Collection<Folder> userFolders;
		Folder folder;

		folders = this.folderService.findAll();
		folder = null;
		actor = null;

		for (final Folder f : folders)
			if (f.getType().getFolderType().equals(FolderType.USERBOX)) {
				actor = f.getActor();
				folder = f;
				break;
			}

		Assert.notNull(folder);
		Assert.notNull(actor);

		userFolders = this.folderService.getUserFolders(actor.getId());

		Assert.isTrue(userFolders.contains(folder));
	}
	@Test(expected = java.lang.IllegalArgumentException.class)
	public void checkFolderTreeIntegrityTest() {
		Folder folder;
		Folder childFolder;
		List<Folder> userFolders;
		List<Folder> childrenFolders;
		Actor actor;

		folder = null;
		actor = null;
		userFolders = new ArrayList<Folder>();
		childrenFolders = new ArrayList<Folder>();
		for(Actor a: actorService.findAll()){
			userFolders = new ArrayList<Folder>(this.folderService.getUserParentFolders(a.getId()));
			for(Folder f: userFolders){
			childrenFolders = new ArrayList<Folder>(this.folderService.getChildrenFolders(f.getId()));
				if(childrenFolders.size()>0){
					folder = f;
					break;
				}
			}
			if(folder!=null&&a.getFolders().contains(folder)){
				actor = a;
				break;
			}
		}
		
		this.authenticate(actor.getUserAccount().getUsername());
		
		if(childrenFolders.size()>0){
			childFolder = childrenFolders.get(0);

			folder.setParentFolder(childFolder);
			this.folderService.save(folder);
		}

		this.authenticate(null);
	}


}
