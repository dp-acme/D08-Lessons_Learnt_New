
package services;

import java.util.Collection;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import security.LoginService;
import security.UserAccount;
import utilities.AbstractTest;
import domain.CreditCard;
import domain.Sponsor;
import domain.Sponsorship;
import domain.Trip;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
	"classpath:spring/datasource.xml", "classpath:spring/config/packages.xml"
})
@Transactional
public class SponsorshipServiceTest extends AbstractTest {

	//Service under test
	@Autowired
	private SponsorshipService	sponsorshipService;

	//Other services
	@Autowired
	private SponsorService		sponsorService;

	@Autowired
	private TripService			tripService;

	@Autowired
	private ActorService		actorService;


	@Test
	public void testCreate() {
		Sponsorship sponsorship;
		Sponsor sponsor;
		Trip trip;

		//Creamos una lista y metemos todos los trip y todos los sponsor en otra

		List<Sponsor> sponsors;
		List<Trip> trips;

		sponsors = (List<Sponsor>) this.sponsorService.findAll();
		trips = (List<Trip>) this.tripService.findAll();

		//cogemos un trip / sponsor
		sponsor = sponsors.get(0);
		trip = trips.get(0);

		sponsorship = this.sponsorshipService.create(sponsor, trip);

		//seteamos los creates

		sponsorship.setBanner(null);
		sponsorship.setInfo(null);
		
		sponsorship.setCreditCard(null);
	}

	@Test
	public void testFindAll() {

		Collection<Sponsorship> result;

		result = this.sponsorshipService.findAll();

		Assert.notNull(result);
	}

	@Test
	public void testFindOne() {

		List<Sponsorship> all;
		Sponsorship sponsorshipFromList, sponsorshipFromRepository;

		all = (List<Sponsorship>) sponsorshipService.findAll();
		Assert.notEmpty(all);
		sponsorshipFromList = all.get(0);

		sponsorshipFromRepository = sponsorshipService.findOne(sponsorshipFromList.getId());

		Assert.notNull(sponsorshipFromRepository);
		Assert.isTrue(sponsorshipFromList.equals(sponsorshipFromRepository));

	}

	@Test
	public void testDelete() {

	}

	@Test
	public void testSave() {
		Sponsorship sponsorship;
		Sponsor sponsor;
		UserAccount principal;
		Trip trip;
		CreditCard creditCard;
		this.authenticate("sponsor1");

		principal = LoginService.getPrincipal();
		sponsor = (Sponsor) actorService.findByUserAccountId(principal.getId());

		creditCard = null; // inicializamos la creditCard
		trip = null;
		for (Sponsorship p : sponsor.getSponsorships()) {
			trip = p.getTrip();
			creditCard = p.getCreditCard();
		}
		sponsorship = this.sponsorshipService.create(sponsor, trip);

		//Creamos una creditcard

		//Modificamos atributos de sponsorship
		sponsorship.setBanner("https://www.google.es/");
		sponsorship.setInfo("https://www.google.es/");
		sponsorship.setCreditCard(creditCard);

	}

}
