<%--
 * action-1.jsp
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>

<!-- TITULO -->

<form:form action="sponsorship/sponsor/edit.do"
	modelAttribute="sponsorship">

	<!-- ATRIBUTOS QUE NO VAMOS A MODIFICAR -->
	<form:hidden path="id" />
	<form:hidden path="version" />
	<form:hidden path="trip" />
	<form:hidden path="sponsor" />

	<jstl:choose>
		<jstl:when test="${param.sponsorshipId!=null}">
			<form:hidden path="creditCard" />
		</jstl:when>
	</jstl:choose>

	<!-- ATRIBUTOS QUE VAMOS A MODIFICAR -->
	<b><spring:message code="sponsorship.editSponsorship" /></b>
	<br />
	<br />

	<!-- BANNER -->
	<form:label path="banner">
		<spring:message code="sponsorship.banner" />
	</form:label>
	<form:input path="banner" />
	<form:errors cssClass="error" path="banner"></form:errors>

	<br />

	<!-- INFO PAGE -->
	<form:label path="info">
		<spring:message code="sponsorship.info" />
	</form:label>
	<form:input path="info" />
	<form:errors cssClass="error" path="info"></form:errors>
	<br />

	<br />

	<!-- CREDITCARD -->
	<b><spring:message code="sponsorship.editSponsorshipC" /></b>
	<br />
	<br />

	<form:label path="creditCard.holderName">
		<spring:message code="sponsorship.creditCard.holderName" />
	</form:label>
	<form:input path="creditCard.holderName" disabled="${disable}" />
	<form:errors cssClass="error" path="creditCard.holderName"></form:errors>
	<br />

	<form:label path="creditCard.brandName">
		<spring:message code="sponsorship.creditCard.brandName" />
	</form:label>
	<form:input path="creditCard.brandName" disabled="${disable}" />
	<form:errors cssClass="error" path="creditCard.brandName"></form:errors>
	<br />

	<form:label path="creditCard.number">
		<spring:message code="sponsorship.creditCard.numberHeader" />
	</form:label>
	<form:input type="number" path="creditCard.number" disabled="${disable}" />
	<form:errors cssClass="error" path="creditCard.number"></form:errors>
	<br />

	<form:label path="creditCard.expirationMonth">
		<spring:message code="sponsorship.creditCard.expirationMonth" />
	</form:label>
	<form:input type="number" path="creditCard.expirationMonth" disabled="${disable}" />
	<form:errors cssClass="error" path="creditCard.expirationMonth"></form:errors>
	<br />

	<form:label path="creditCard.expirationYear">
		<spring:message code="sponsorship.creditCard.expirationYear" />
	</form:label>
	<form:input  type="number" path="creditCard.expirationYear" disabled="${disable}" />
	<form:errors cssClass="error" path="creditCard.expirationYear"></form:errors>
	<br />

	<form:label path="creditCard.cvv">
		<spring:message code="sponsorship.creditCard.cvv" />
	</form:label>
	<form:input  type="number" path="creditCard.cvv" disabled="${disable}" />
	<form:errors cssClass="error" path="creditCard.cvv"></form:errors>
	<br />

	<br />
	<!-- BOTONES -->
	<!-- SAVE -->
	<input type="submit" name="save"
		value="<spring:message code="sponsorship.save"/>" />
	&nbsp;
	&nbsp;
	
	<!-- CANCEL -->

	<jstl:choose>
		<jstl:when test="${param.sponsorshipId!=null}">
			<input type="button" name="cancel"
				value="<spring:message code="sponsorship.cancel" />"
				onclick="javascript: relativeRedir('sponsorship/sponsor/list.do');" />
			<br />
		</jstl:when>
		<jstl:otherwise>
		<input type="button" name="cancel"
				value="<spring:message code="sponsorship.cancel" />"
				onclick="javascript: relativeRedir('trip/list.do');" />
			<br />
		</jstl:otherwise>
	</jstl:choose>




</form:form>
