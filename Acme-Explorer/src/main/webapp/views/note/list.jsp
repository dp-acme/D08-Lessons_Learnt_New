<%--
 * action-1.jsp
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl"	uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>


<jsp:useBean id="date" class="java.util.Date" />
<security:authorize access="hasRole('MANAGER')">
	<jstl:set var="uri" value="note/manager/list.do"/>
</security:authorize>
<security:authorize access="hasRole('AUDITOR')">
	<jstl:set var="uri" value="note/auditor/list.do"/>
</security:authorize>

<security:authorize access="hasAnyRole('MANAGER', 'AUDITOR')">
	<display:table name="notes" id="row" requestURI="${uri}" pagesize="5">
		
		<spring:message code="note.list.tripHeader" var="tripHeader" />
		<display:column property="trip.ticker" title="${tripHeader}" sortable="true" />
		
		<spring:message code="note.list.timeAuditorHeader" var="timeAuditorHeader" />
		<display:column title="${timeAuditorHeader}" sortable="true">
			<spring:message code="note.formatDate" var="formatDate"/>
			<fmt:formatDate value="${row.getTimeAuditor()}" pattern="${formatDate}"/>
		</display:column>
		
		<spring:message code="note.list.auditorRemarkHeader" var="auditorRemarkHeader" />
		<display:column property="auditorRemark" title="${auditorRemarkHeader}" sortable="false" />
		
		<spring:message code="note.list.timeManagerHeader" var="timeManagerHeader" />
		<display:column title="${timeManagerHeader}" sortable="true">
			<spring:message code="note.formatDate" var="formatDate"/>
			<fmt:formatDate value="${row.getTimeManager()}" pattern="${formatDate}"/>
		</display:column>
		
		<spring:message code="note.list.managerReplyHeader" var="managerReplyHeader" />
		<display:column property="managerReply" title="${managerReplyHeader}" sortable="false"/>
		
		<security:authorize access="hasRole('MANAGER')">
			<spring:message code="note.list.managerReply" var="managerReply"/>
			<display:column title="" sortable="false">
					<jstl:if test="${empty row.getManagerReply()}">
						<spring:url value="note/manager/edit.do?noteId=${row.getId()}" var="editManager"/>
						<a href="${editManager}">
							<jstl:out value="${managerReply}" />
						</a>
					</jstl:if>
			</display:column>
		</security:authorize>
	</display:table>
	
	<spring:message code="note.list.back" var="listBack"/>
	<input type="button" value="${listBack}" 
		onClick="javascript: relativeRedir('trip/list.do');" /> 
</security:authorize>