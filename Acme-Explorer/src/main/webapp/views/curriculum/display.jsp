<%--
 * action-1.jsp
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>


<jstl:choose>
	<jstl:when test="${not empty curriculum}">
		<security:authorize access="hasRole('RANGER')">
			<a href="curriculum/ranger/edit.do?curriculumId=${curriculum.id}"><spring:message
					code="curriculum.display.edit.curriculum" /></a>
		</security:authorize>
		<br />
		<br />
		<!-- PHOTO -->
		<spring:message code="curriculum.display.photo" />:
		<br />
		<img src="${curriculum.photo}" height="150" width="150" />

		<br />
		<br />

		<!-- NAME -->
		<spring:message code="curriculum.display.name" />
			:
		<jstl:out value="${curriculum.name}"></jstl:out>

	&nbsp;&nbsp;


		<!-- PROFILE -->
		<spring:message code="curriculum.display.profile" />
:
<jstl:out value="${curriculum.profile}"></jstl:out>
		<br />
		<br />

		<!-- EMAIL -->
		<spring:message code="curriculum.display.email" />
:
<jstl:out value="${curriculum.email}"></jstl:out>
		<br />
		<br />
		<!-- PHONE -->
		<spring:message code="curriculum.display.phone" />
:
<jstl:out value="${curriculum.phone}"></jstl:out>

		<br />
		<br />


		<!-- TICKER -->
		<spring:message code="curriculum.display.ticker" />
:
<jstl:out value="${curriculum.ticker}"></jstl:out>

		<br />
		<br />


		<!-- LINKS -->
		<security:authorize
			access="hasAnyRole('MANAGER','AUDITOR','ADMINISTRATOR','SPONSOR','EXPLORER') or isAnonymous()">


			<a href="education/list.do?curriculumId=${param.curriculumId}"><spring:message
					code="curriculum.display.education.show" /></a>
	&nbsp;
		<a href="miscellaneous/list.do?curriculumId=${param.curriculumId}"><spring:message
					code="curriculum.display.miscellaneous.show" /></a>
			<br />
			<br />
			<a href="endorser/list.do?curriculumId=${param.curriculumId}"><spring:message
					code="curriculum.display.endorser.show" /></a>
	&nbsp;
	<a href="professional/list.do?curriculumId=${param.curriculumId}"><spring:message
					code="curriculum.display.professional.show" /></a>
		</security:authorize>

		<!-- AUTHENTICATE -->
		<security:authorize access="hasRole('RANGER')">
			<a href="education/ranger/list.do"><spring:message
					code="curriculum.display.education.show" /></a>
	&nbsp;
		<a href="miscellaneous/ranger/list.do"><spring:message
					code="curriculum.display.miscellaneous.show" /></a>
			<br />
			<br />
			<a href="endorser/ranger/list.do"><spring:message
					code="curriculum.display.endorser.show" /></a>
	&nbsp;
		<a href="professional/ranger/list.do"><spring:message
					code="curriculum.display.professional.show" /></a>
		</security:authorize>

		<br />
		<br />
	</jstl:when>
	<jstl:otherwise>
		<a href="curriculum/ranger/create.do"><spring:message
				code="curriculum.display.create" /></a>
		<br />
		<br />
	</jstl:otherwise>
</jstl:choose>
<!-- FOR BUTTON BACK -->
<!-- DIFFERENT URL FOR UNAUTHENTICATE AND AUTHENTICATE -->
<security:authorize access="hasRole('RANGER')">
	<spring:url var="urlListTrip" value="trip/list.do" />
</security:authorize>

<security:authorize access="isAnonymous()">
	<spring:url var="urlListTrip" value="trip/list.do" />
</security:authorize>

<input type="button" name="back"
	value="<spring:message code="curriculum.display.back" />"
	onclick="javascript: relativeRedir('${urlListTrip}');" />

