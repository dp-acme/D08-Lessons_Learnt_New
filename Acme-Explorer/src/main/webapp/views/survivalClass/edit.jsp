<%--
 * edit.jsp
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>

<jstl:choose>
	<jstl:when test="${param.survivalClassId==null}">
		<h1>
			<spring:message code="survivalClass.manager.create" />
		</h1>
	</jstl:when>
	<jstl:otherwise>
		<h1>
			<spring:message code="survivalClass.manager.edit" />
		</h1>
	</jstl:otherwise>
</jstl:choose>


<form:form
	action="survivalClass/manager/edit.do?${param.survivalClassId}"
	modelAttribute="survivalClass">

	<!-- ATRIBUTOS QUE NO SE VAN A MODIFICAR -->
	<form:hidden path="id" />
	<form:hidden path="version" />
	<form:hidden path="explorers" />
	<%-- 	<form:hidden path="manager" /> --%>
	<form:hidden path="trip" />

	<!-- TITLE -->
	<form:label path="title">
		<spring:message code="survivalClass.title" />:
	</form:label>
	<form:input path="title" />
	<form:errors cssClass="error" path="title" />

	<br />
	<br />
	<!-- DESCIPCION -->
	<form:label path="description">
		<spring:message code="survivalClass.description" />:
	</form:label>
	<form:input path="description" />
	<form:errors cssClass="error" path="description" />

	<br />
	<br />

	<!-- MOMENT -->
	<form:label path="moment">
		<spring:message code="survivalClass.moment" />:
	</form:label>
	<form:input path="moment" placeholder="dd/MM/yyyy HH:mm" />
	<form:errors cssClass="error" path="moment" />

	<br />
	<br />

	<!-- LONGITUDE -->
	<form:label path="location.longitude">
		<spring:message code="survivalClass.location.longitude" />:
	</form:label>
	<form:input path="location.longitude" />
	<form:errors cssClass="error" path="location.longitude" />

	<br />
	<br />
	<!-- Latitude -->
	<form:label path="location.latitude">
		<spring:message code="survivalClass.location.latitude" />:
	</form:label>
	<form:input path="location.latitude" />
	<form:errors cssClass="error" path="location.latitude" />

	<br />
	<br />
	
		<!-- Name  -->
	<form:label path="location.name">
		<spring:message code="survivalClass.location.name" />:
	</form:label>
	<form:input path="location.name" />
	<form:errors cssClass="error" path="location.name" />

	<br />
	<br />
	<!-- BOTONES -->

	<!-- BOTON DE SAVE -->
	<input type="submit" name="save"
		value="<spring:message code="survivalClass.save"/>" />
	<!-- PREGUNTAMOS POR LOS CASOS QUE TENEMOS -->
	<!-- VAMOS A EDITARLO -->
	<jstl:if test="${survivalClass.id !=0}">
		<!-- QUIERE DECIR QUE HAY OBJETOS  -->
		<!-- PODEMOS PONER BOTON DE BORRAR -->
		<input type="submit" name="delete"
			value="<spring:message code="survivalClass.delete" />"
			onclick="javascript: return confirm('<spring:message code="survivalClass.confirm.delete" />')" />
		&nbsp;
	</jstl:if>

		<jstl:choose>
			<jstl:when test="${param.survivalClassId!=null}">
				<input type="button" name="cancel"
					value="<spring:message code="survivalClass.cancel" />"
					onclick="javascript: relativeRedir('survivalClass/manager/list.do');" />
				<br />
			</jstl:when>
			<jstl:otherwise>
				<input type="button" name="cancel"
					value="<spring:message code="survivalClass.cancel" />"
					onclick="javascript: relativeRedir('trip/manager/list.do');" />
				<br />
			</jstl:otherwise>
		</jstl:choose>

	<br />
</form:form>
