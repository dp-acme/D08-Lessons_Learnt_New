<%--
 * action-1.jsp
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>


<form:form modelAttribute="tagValue" action="tagvalue/manager/create.do?tripId=${tripId}">
	<form:hidden path="id"/>
	
	<form:label path="value">
		<spring:message code="tagValue.list.valueHeader" />
	</form:label>
	<form:input path="value" />
	<form:errors path="value" cssClass="error"/>
	
	<br/>
	<form:label path="tagKey">
		<spring:message code="tagValue.list.tagKeyHeader" />
	</form:label>
	<form:select path="tagKey">
		<form:option label="------" value="0" />
		<form:options items="${tagKeys}" itemLabel="name" itemValue="id" />
	</form:select>
	<form:errors path="tagKey" cssClass="error"/>
	
	<br/>
	<spring:message code= "tagValue.create.save" var="saveButton"/>
	<input type="submit" name="save"
		value="${saveButton}" />
		
	<br/>
	<spring:message code= "tagValue.create.cancel" var="cancelButton"/>
	<input type="button" name="cancel"
		value="${cancelButton}"
		onClick="javascript: relativeRedir('trip/manager/list.do');" />
</form:form>