<%--
 * action-1.jsp
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl"	uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<display:table name="tagValues" id="row" requestURI="${requestUri}" pagesize="5">
	
		<spring:message code="tagValue.list.tagKeyHeader" var="tagKeyHeader" />
		<display:column property="tagKey.name" title="${tagKeyHeader}" sortable="true" />
		
		<spring:message code="tagValue.list.valueHeader" var="valueHeader" />
		<display:column property="value" title="${valueHeader}" sortable="true" />
		
		<security:authorize access="hasRole('MANAGER')">
			<security:authentication property="principal.username" var="principal" />
			<jstl:if test="${trip.getManager().getUserAccount().getUsername() == principal}"><
				<spring:message code="tagValue.list.deleteTagValueHeader" var="deleteTagValueHeader" />
				<display:column title="" sortable="false">
					<spring:url value="tagvalue/manager/delete.do?tagvalueId=${row.getId()}&tripId=${trip.id}"
						var="deleteTagValue" />
					<a href="${deleteTagValue}"> <jstl:out value="${deleteTagValueHeader}" />
					</a>
		</display:column>
			</jstl:if>
		</security:authorize>
	
</display:table>

<br />
<security:authorize access = "hasAnyRole('ADMINISTRATOR','RANGER') or isAnonymous()">
	<input type="button" name="back"
			value="<spring:message code="backText" />"
			onclick="javascript: relativeRedir('trip/display.do?tripId=${trip.getId()}');" />
	</security:authorize>

<security:authorize access = "hasRole('AUDITOR')">
	<input type="button" name="back"
			value="<spring:message code="backText" />"
			onclick="javascript: relativeRedir('trip/auditor/display.do?tripId=${trip.getId()}');" />
</security:authorize>


<security:authorize access = "hasRole('SPONSOR')">
	<input type="button" name="back"
			value="<spring:message code="backText" />"
			onclick="javascript: relativeRedir('trip/sponsor/display.do?tripId=${trip.getId()}');" />
</security:authorize>

<security:authorize access = "hasRole('EXPLORER')">
	<input type="button" name="back"
			value="<spring:message code="backText" />"
			onclick="javascript: relativeRedir('trip/explorer/display.do?tripId=${trip.getId()}');" />
</security:authorize>

<security:authorize access = "hasRole('MANAGER')">
	<input type="button" name="back"
			value="<spring:message code="backText" />"
			onclick="javascript: relativeRedir('trip/manager/display.do?tripId=${trip.getId()}');" />
</security:authorize>