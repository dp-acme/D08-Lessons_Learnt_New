<%--
 * action-1.jsp
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>

<input type="hidden" id="messagePhone"
	value="<spring:message code="emergencyContact.edit.phone.message"/>" />
<input type="hidden" id="countryCode"
	value="${countryCode}" />

<script type="text/javascript">
	function checkPhone() {
		phone = document.getElementById("phoneInput");
		res = true;
		
		if (!/^(\+[0-9]{1,3}\s)?(\([0-9]{1,3}\))?\s?[0-9]{4,}$/.test(phone.value)&&phone.value) {
			res = confirm(document.getElementById("messagePhone").value);
		}else if (/^[0-9]{4,}$/.test(phone.value)&&phone.value) {
			phoneCountry = document.getElementById("countryCode").value+ ' ' + document.getElementById("phoneInput").value ;
	 		newPhone = document.getElementById("phoneInput");
			newPhone.value = phoneCountry;
			
		} 
		return res;
	}
</script>

<form:form method="POST" action="emergencyContact/edit.do?explorerId=${explorer.getId()}" modelAttribute="emergencyContact">

	<form:hidden path="id"/>
	<form:hidden path="version"/>
	<input type="hidden" name="explorerId" value="${explorer.getId()}" />

	<form:label path="name">
		<spring:message code="emergencyContact.edit.name" />
	</form:label>
	<form:input path="name" />
	<form:errors path="name" cssClass="error" />
	
	<br/>
	
	<form:label path="email">
		<spring:message code="emergencyContact.edit.email" />
	</form:label>
	<form:input path="email" placeholder="email@domain.com" />
	<form:errors path="email" cssClass="error" />
	
	<br/>
	
	<form:label path="phone">
		<spring:message code="emergencyContact.edit.phone" />
	</form:label>
	<form:input path="phone" id="phoneInput" />
	<form:errors path="phone" cssClass="error" />

	<br/>
	
	<input type="submit" name="save" value="<spring:message code="emergencyContact.edit.save" />" 
	onClick="javascript: return checkPhone();" />
	
	<jstl:if test="${not empty param.emergencyContactId}">	
		<input type="submit" name="delete" value="<spring:message code="emergencyContact.edit.delete" />"
		onClick="javascript: return confirm('<spring:message code="emergencyContact.edit.deleteConfirm" />');" />
	</jstl:if>	

	<input type="button" value="<spring:message code="emergencyContact.edit.cancel" />" 
		onClick="javascript: relativeRedir('emergencyContact/list.do?explorerId=${explorer.getId()}');" />

</form:form>


