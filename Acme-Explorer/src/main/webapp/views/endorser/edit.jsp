<%--
 * edit.jsp
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>

<input type="hidden" id="messagePhone"
	value="<spring:message code="endorser.phone.message"/>" />
<input type="hidden" id="countryCode"
	value="${countryCode}" />

<script type="text/javascript">
	function checkPhone() {
		phone = document.getElementById("phone");
		res = true;
		
		if (!/^(\+[0-9]{1,3}\s)?(\([0-9]{1,3}\))?\s?[0-9]{4,}$/.test(phone.value)&&phone.value) {
			res = confirm(document.getElementById("messagePhone").value);
		}else if (/^[0-9]{4,}$/.test(phone.value)&&phone.value) {
			phoneCountry = document.getElementById("countryCode").value+ ' ' + document.getElementById("phone").value ;
	 		newPhone = document.getElementById("phone");
			newPhone.value = phoneCountry;
			
		} 
		return res;
	}
</script>
<jstl:choose>
	<jstl:when test="${param.endorserId==null}">
		<h1>
			<spring:message code="endorser.ranger.create" />
		</h1>
		<jstl:set var="disable" value="false" />
	</jstl:when>
	<jstl:otherwise>
		<h1>
			<spring:message code="endorser.ranger.edit" />
		</h1>
		<jstl:set var="disable" value="true" />
	</jstl:otherwise>
</jstl:choose>


<form:form action="endorser/ranger/edit.do" modelAttribute="endorser">

	<!-- ATRIBUTOS QUE NO SE VAN A MODIFICAR -->
	<form:hidden path="id" />
	<form:hidden path="version" />
	<form:hidden path="curriculum" />

	<!-- ATRIBUTOS QUE VAMOS A MODIFICAR -->
	<b><spring:message code="endorser.editEndorser" /></b>
	<br />
	<br />

	<!-- FULLNAME -->
	<form:label path="fullName">
		<spring:message code="endorser.fullName" />:
	</form:label>
	<form:input path="fullName" />
	<form:errors cssClass="error" path="fullName" />

	<br />
	<br />
	<!-- EMAIL -->
	<form:label path="email">
		<spring:message code="endorser.email" />:
	</form:label>
	<form:input path="email" />
	<form:errors cssClass="error" path="email" />

	<br />
	<br />

	<!-- PHONE -->
	<form:label path="phone">
		<spring:message code="endorser.phone" />:
	</form:label>
	<form:input path="phone" />
	<form:errors cssClass="error" path="phone" />

	<br />
	<br />

	<!-- PROFILE -->
	<form:label path="profile">
		<spring:message code="endorser.profile" />:
	</form:label>
	<form:input path="profile" />
	<form:errors cssClass="error" path="profile" />

	<br />
	<br />
	<!-- PARA EL SALTO DE LINEA -->
	<!-- COMMENTS -->
	<form:label path="comments">
		<spring:message code="endorser.comments" />:
	</form:label>
	<form:input path="comments" />
	<form:errors cssClass="error" path="comments" />

	<br />
	<br />
	<!-- BOTONES -->

	<!-- BOTON DE SAVE -->
	<input type="submit" name="save"
		value="<spring:message code="endorser.save"/>" 	onClick="javascript: return checkPhone();" />
	<!-- PREGUNTAMOS POR LOS CASOS QUE TENEMOS -->
	<!-- VAMOS A EDITARLO -->
	<jstl:if test="${endorser.id !=0}">
		<!-- QUIERE DECIR QUE HAY OBJETOS  -->
		<!-- PODEMOS PONER BOTON DE BORRAR -->
		<input type="submit" name="delete"
			value="<spring:message code="endorser.delete" />"
			onclick="javascript: return confirm('<spring:message code="endorser.confirm.delete" />')" />
		&nbsp;<!-- separacion a la derecha -->
	</jstl:if>

	<jstl:choose>
		<jstl:when test="${param.endorserId!=null}">
			<input type="button" name="cancel"
				value="<spring:message code="endorser.cancel" />"
				onclick="javascript: relativeRedir('endorser/ranger/list.do');" />
			<br />
		</jstl:when>
		<jstl:otherwise>
			<input type="button" name="cancel"
				value="<spring:message code="endorser.cancel" />"
				onclick="javascript: relativeRedir('trip/ranger/list.do');" />
			<br />
		</jstl:otherwise>
	</jstl:choose>
</form:form>
<!-- FIN DE CREACION DE FORMULARIO -->