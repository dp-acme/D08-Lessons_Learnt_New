<%--
 * action-1.jsp
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>



<form:form action="stage/manager/create.do" modelAttribute="stage">
	<form:hidden path="trip"/>
	<form:hidden path="id"/>
	
	<br/>
	<form:label path="title">
		<spring:message code="stage.create.title" />
	</form:label>
	<form:input path="title" />
	<form:errors path="title" cssClass="error"/>
	
	<br/>
	<form:label path="description">
		<spring:message code="stage.create.description" />
	</form:label>
	<form:input path="description" />
	<form:errors path="description" cssClass="error"/>
	
	<br/>
	<form:label path="price">
		<spring:message code="stage.create.price" />
	</form:label>
	<form:input path="price" />
	<form:errors path="price" cssClass="error"/>
	
	<br/>
	<spring:message code= "stage.create.save" var="saveButton"/>
	<input type="submit" name="save"
		value="${saveButton}" />
	
	<spring:message code= "stage.create.cancel" var="cancelButton"/>
	<input type="button" name="cancel"
		value="${cancelButton}"
		onClick="javascript: relativeRedir('trip/manager/display.do?tripId=${trip.getId()}');" />
</form:form>