package repositories;

import java.util.Collection;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import domain.TagKey;
import domain.TagValue;

@Repository
public interface TagValueRepository extends JpaRepository<TagValue, Integer>{

	@Query("select tv from TagValue tv where tv.tagKey = ?1")
	Collection<TagValue> filterByTagKey(TagKey tagKey);

	@Query("select count(tv) from Trip t join t.tags tv where tv.id = ?1")
	Integer getTagValueReferences(int tagvalueId);
	
}
