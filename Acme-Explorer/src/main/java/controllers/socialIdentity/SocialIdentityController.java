/*
 * AdministratorController.java
 * 
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the
 * TDG Licence, a copy of which you may download from
 * http://www.tdg-seville.info/License.html
 */

package controllers.socialIdentity;


import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import security.LoginService;
import services.ActorService;
import services.SocialIdentityService;
import controllers.AbstractController;
import domain.Actor;
import domain.SocialIdentity;

@Controller
@RequestMapping("/socialIdentity")
public class SocialIdentityController extends AbstractController {

	// Services ---------------------------------------------------------------
	
	@Autowired
	private SocialIdentityService socialIdentityService;
	
	@Autowired
	private ActorService actorService;
	
	
	
	// Constructors -----------------------------------------------------------

	public SocialIdentityController() {
		super();
	}
	
	@RequestMapping(value="/create", method = RequestMethod.GET)
	public ModelAndView create(@RequestParam int actorId){
		ModelAndView result;
		SocialIdentity socialIdentity;
		
		socialIdentity = this.socialIdentityService.create(actorId);
		result = this.createEditModelAndView(socialIdentity);

		
		return result;
	}
	
	@RequestMapping(value="/delete", method = RequestMethod.GET)
	public ModelAndView delete(@RequestParam int socialIdentityId){
		ModelAndView result;
		SocialIdentity socialIdentity;
		
		socialIdentity = socialIdentityService.findOne(socialIdentityId);
		
		try {
			String rol;
			
			rol = LoginService.getPrincipal().getAuthorities().toArray()[0].toString().toLowerCase();
			socialIdentityService.delete(socialIdentity);
			result = new ModelAndView("redirect:/"+ rol +"/" +rol +"/edit.do");
		} catch (Throwable oops) {
			result = deleteModelAndView(socialIdentity, "socialIdentity.commit.error");
		}
	return result;
	}
	
	
	private ModelAndView deleteModelAndView(SocialIdentity socialIdentity,
			String messageCode) {
		ModelAndView result;
		String rol;
		Actor actor;
		
		actor=  actorService.findPrincipal();
		rol = LoginService.getPrincipal().getAuthorities().toArray()[0].toString().toLowerCase();
		result = new ModelAndView(rol +"/" +rol +"/edit");
		result.addObject(rol, actor);
		result.addObject("socialIdentities", actor.getSocialIdentities());
		result.addObject("actorId", actor.getId());
		result.addObject("messageSocialId",messageCode);
		
		return result;
	}

	@RequestMapping(value = "/create", method = RequestMethod.POST, params="save")
	public ModelAndView save(@Valid SocialIdentity socialIdentity, BindingResult binding){
		ModelAndView result;
		if(binding.hasErrors()){
			result = createEditModelAndView(socialIdentity);
		} else { 
			try {
				String rol;
				
				rol = LoginService.getPrincipal().getAuthorities().toArray()[0].toString().toLowerCase();
				socialIdentityService.save(socialIdentity);
				result = new ModelAndView("redirect:/"+ rol +"/" +rol +"/edit.do");
			} catch (Throwable oops) {
				result = createEditModelAndView(socialIdentity, "socialIdentity.commit.error");
			}
		}
		return result;
	}
	
	
	protected ModelAndView createEditModelAndView(SocialIdentity socialIdentity) {
		ModelAndView result; 
		
		result = createEditModelAndView(socialIdentity, null);
		
		return result;
	}


	protected ModelAndView createEditModelAndView(SocialIdentity socialIdentity,
			String messageCode) {
		ModelAndView result;
		result = new ModelAndView("socialIdentity/create");
		result.addObject("socialIdentity", socialIdentity);
		
		result.addObject("message", messageCode);
		
		return result;
	}

}
