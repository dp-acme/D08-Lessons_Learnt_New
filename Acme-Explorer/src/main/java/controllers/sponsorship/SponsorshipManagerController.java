package controllers.sponsorship;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import services.SponsorshipService;
import controllers.AbstractController;
import domain.Sponsorship;

@Controller
@RequestMapping("/sponsorship")
public class SponsorshipManagerController extends AbstractController {

	@Autowired
	private SponsorshipService sponsorshipService;

	@RequestMapping("/manager/list")
	public ModelAndView list(@RequestParam int tripId) {
		ModelAndView result;
		Collection<Sponsorship> sponshorships;
				
		sponshorships = sponsorshipService.getSponsorshipsByTrip(tripId);

		result = new ModelAndView("sponsorship/manager/list");
		result.addObject("sponsorships", sponshorships);

		return result;
	}

}
