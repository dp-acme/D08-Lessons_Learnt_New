/*
 * AdministratorController.java
 * 
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the
 * TDG Licence, a copy of which you may download from
 * http://www.tdg-seville.info/License.html
 */

package controllers.manager;


import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import controllers.AbstractController;

import services.ActorService;
import services.ConfigurationService;
import services.ManagerService;
import domain.Actor;
import domain.Manager;

@Controller
@RequestMapping("/manager/manager")
public class ManagerManagerController extends AbstractController {

	// Services ---------------------------------------------------------------
	
	@Autowired
	private ManagerService managerService;
	
	@Autowired
	private ActorService actorService;

	@Autowired
	private ConfigurationService configurationService;
	
	// Constructors -----------------------------------------------------------

	public ManagerManagerController() {
		super();
	}
	
	@RequestMapping(value="/edit", method = RequestMethod.GET)
	public ModelAndView edit(){
		ModelAndView result;
		Actor principal;
		Manager manager;
		
		principal = actorService.findPrincipal();
		Assert.isTrue(principal instanceof Manager);
		manager = (Manager) principal;
		Assert.notNull(manager);
		result = this.createEditModelAndView(manager);
		
		return result;
	}
	
	
	
	
	@RequestMapping(value = "/edit", method = RequestMethod.POST, params="save")
	public ModelAndView save(@Valid Manager manager, BindingResult binding){
		ModelAndView result;
		if(binding.hasErrors()){
			result = createEditModelAndView(manager);
		} else { 
			try {
				managerService.save(manager);
				result = new ModelAndView("redirect:/");
			} catch (Throwable oops) {
				result = createEditModelAndView(manager, "manager.commit.error");
			}
		}
		return result;
	}
	
	protected ModelAndView createEditModelAndView(Manager manager) {
		ModelAndView result; 
		
		result = createEditModelAndView(manager, null);
		
		return result;
	}


	protected ModelAndView createEditModelAndView(Manager manager,
			String messageCode) {
		ModelAndView result;
		
		result = new ModelAndView("manager/manager/edit");
		result.addObject("manager", manager);	
		result.addObject("actorId", manager.getId());
		result.addObject("countryCode", configurationService.findConfiguration().getCountryCode());

		result.addObject("socialIdentities", manager.getSocialIdentities());
		result.addObject("message", messageCode);
		
		return result;
	}

}
