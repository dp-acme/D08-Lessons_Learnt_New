/*
 * AdministratorController.java
 * 
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the
 * TDG Licence, a copy of which you may download from
 * http://www.tdg-seville.info/License.html
 */

package controllers.auditRecord;


import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import controllers.AbstractController;

import services.AuditRecordService;
import domain.AuditRecord;

@Controller
@RequestMapping("/auditrecord")
public class AuditRecordController extends AbstractController {

	// Services ---------------------------------------------------------------
	
	@Autowired
	private AuditRecordService auditRecordService;
	
	// Constructors -----------------------------------------------------------

	public AuditRecordController() {
		super();
	}

	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public ModelAndView listByTrip(@RequestParam int tripId) {
		ModelAndView result;
		Collection<AuditRecord> auditRecords;

		auditRecords = auditRecordService.findAllByTrip(tripId);
		
		
		result = new ModelAndView("auditrecord/list");
		result.addObject("auditRecords", auditRecords);
		result.addObject("requestURI", "auditrecord/auditor/list.do");

		return result;
	}
}
