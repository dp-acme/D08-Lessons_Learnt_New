
package controllers.curriculum;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import controllers.AbstractController;

import services.CurriculumService;
import services.MiscellaneousService;
import domain.Curriculum;
import domain.Miscellaneous;

@Controller
@RequestMapping("/miscellaneous")
public class MiscellaneousController extends AbstractController {

	//Services------------------------------------------------------------
	@Autowired
	private MiscellaneousService	miscellaneousService;

	@Autowired
	private CurriculumService		curriculumService;


	//Constructors------------------------------------------------------------
	private MiscellaneousController() {
		super();
	}

	//Display-------------------------------------------------------------------------------
	@RequestMapping(value = "/display", method = RequestMethod.GET)
	public ModelAndView display(@RequestParam final int miscellaneousId) {
		ModelAndView result;
		Miscellaneous miscellaneous;

		miscellaneous = this.miscellaneousService.findOne(miscellaneousId);
		Assert.notNull(miscellaneous);

		result = new ModelAndView("miscellaneous/display");
		result.addObject("miscellaneous", miscellaneous);
		result.addObject("curriculumId", miscellaneous.getCurriculum().getId());

		return result;
	}
	//List -------------------------------------------------------------------------
	@RequestMapping("/list")
	public ModelAndView list(@RequestParam int curriculumId) {
		ModelAndView result;
		Collection<Miscellaneous> miscellaneousRecords;
		Curriculum curriculum;
		curriculum = this.curriculumService.findOne(curriculumId);
	
		miscellaneousRecords = curriculum.getMiscellaneousRecords();

		result = new ModelAndView("miscellaneous/list");
		result.addObject("miscellaneousRecords", miscellaneousRecords);

		return result;
	}
}
