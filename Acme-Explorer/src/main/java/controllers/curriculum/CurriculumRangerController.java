
package controllers.curriculum;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import security.LoginService;
import services.ActorService;
import services.ConfigurationService;
import services.CurriculumService;
import controllers.AbstractController;
import domain.Curriculum;
import domain.Ranger;

@Controller
@RequestMapping("/curriculum")
public class CurriculumRangerController extends AbstractController {

	//Services----------------------------------------------------
	@Autowired
	private CurriculumService	curriculumService;
	@Autowired
	private ActorService		actorService;
	
	@Autowired
	private ConfigurationService	configurationService;



	//Constructors----------------------------------------------------
	public CurriculumRangerController() {
		super();
	}
	// Display ----------------------------------------------------------------
	@RequestMapping(value = "/ranger/display", method = RequestMethod.GET)
	public ModelAndView display() {
		ModelAndView result;
		Curriculum curriculum;
		Ranger ranger;

		ranger = (Ranger) this.actorService.findByUserAccountId(LoginService.getPrincipal().getId());

		curriculum = ranger.getCurriculum();

		result = new ModelAndView("curriculum/ranger/display");
		result.addObject("curriculum", curriculum);

		return result;
	}

	@RequestMapping("/ranger/edit")
	public ModelAndView edit() {
		ModelAndView result;
		Curriculum curriculum;
		Ranger ranger;

		ranger = (Ranger) this.actorService.findByUserAccountId(LoginService.getPrincipal().getId());
		curriculum = ranger.getCurriculum();

		Assert.notNull(curriculum);

		result = createEditModelAndView(curriculum);

		return result;
	}

	@RequestMapping(value = "/ranger/edit", method = RequestMethod.POST, params = "delete")
	public ModelAndView delete(final Curriculum curriculum, final BindingResult binding) {
		ModelAndView result;

		try {
			this.curriculumService.delete(curriculum);
			result = new ModelAndView("redirect:display.do");
		} catch (final Throwable oops) {
			result = this.createEditModelAndView(curriculum, "curriculum.commit.error");
		}

		return result;
	}

	@RequestMapping(value = "/ranger/edit", method = RequestMethod.POST, params = "save")
	public ModelAndView save(@Valid Curriculum curriculum, BindingResult binding) {
		ModelAndView result;

		if (binding.hasErrors()) {
			result = createEditModelAndView(curriculum);
		} else {
			try {
				this.curriculumService.save(curriculum);
				result = new ModelAndView("redirect:display.do");
			} catch (Throwable oops) {
				result = createEditModelAndView(curriculum, "curriculum.commit.error");
			}
		}

		return result;
	}

	@RequestMapping("/ranger/create")
	public ModelAndView create() {
		ModelAndView result;
		Curriculum curriculum;

		curriculum = curriculumService.create();

		result = createEditModelAndView(curriculum);

		return result;
	}

	private ModelAndView createEditModelAndView(Curriculum curriculum) {
		ModelAndView result;

		result = createEditModelAndView(curriculum, null);

		return result;
	}

	private ModelAndView createEditModelAndView(Curriculum curriculum, String messageCode) {
		ModelAndView result;

		result = new ModelAndView("curriculum/ranger/edit");

		result.addObject("curriculum", curriculum);
		result.addObject("message", messageCode);
		result.addObject("countryCode", configurationService.findConfiguration().getCountryCode());


		return result;
	}
}
