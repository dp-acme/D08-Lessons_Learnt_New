
package controllers.curriculum;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import controllers.AbstractController;

import services.CurriculumService;
import services.EducationService;
import domain.Curriculum;
import domain.Education;

@Controller
@RequestMapping("/education")
public class EducationController extends AbstractController {

	//Services------------------------------------------------------------
	@Autowired
	private EducationService	educationService;

	@Autowired
	private CurriculumService	curriculumService;


	//Constructors------------------------------------------------------------
	private EducationController() {
		super();
	}

	//Display-------------------------------------------------------------------------------
	@RequestMapping(value = "/display", method = RequestMethod.GET)
	public ModelAndView display(@RequestParam final int educationId) {
		ModelAndView result;
		Education education;

		education = this.educationService.findOne(educationId);
		Assert.notNull(education);

		result = new ModelAndView("education/display");
		result.addObject("education", education);
		result.addObject("curriculumId", education.getCurriculum().getId());

		return result;
	}

	//List -------------------------------------------------------------------------
	@RequestMapping("/list")
	public ModelAndView list(@RequestParam int curriculumId) {
		ModelAndView result;
		Collection<Education> educationRecords;
		Curriculum curriculum;
		curriculum = this.curriculumService.findOne(curriculumId);

		educationRecords = curriculum.getEducationRecords();

		result = new ModelAndView("education/list");
		result.addObject("educationRecords", educationRecords);

		return result;
	}
}
