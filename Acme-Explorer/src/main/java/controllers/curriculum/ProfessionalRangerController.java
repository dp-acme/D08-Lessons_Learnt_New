
package controllers.curriculum;

import java.util.Collection;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import security.LoginService;
import services.ActorService;
import services.ProfessionalService;
import controllers.AbstractController;
import domain.Curriculum;
import domain.Professional;
import domain.Ranger;

@Controller
@RequestMapping("/professional")
public class ProfessionalRangerController extends AbstractController {

	//Services------------------------------------------------------------
	@Autowired
	private ProfessionalService	professionalService;

	@Autowired
	private ActorService		actorService;


	//Constructors------------------------------------------------------------
	private ProfessionalRangerController() {
		super();
	}

	//Display-------------------------------------------------------------------------------
	@RequestMapping(value = "/ranger/display", method = RequestMethod.GET)
	public ModelAndView display(@RequestParam final int professionalId) {
		ModelAndView result;
		Professional professional;

		professional = this.professionalService.findOne(professionalId);
		Assert.notNull(professional);
		result = new ModelAndView("professional/ranger/display");
		result.addObject("professional", professional);

		return result;
	}

	//List -------------------------------------------------------------------------
	@RequestMapping("/ranger/list")
	public ModelAndView list() {
		ModelAndView result;
		Collection<Professional> professionalRecords;
		Ranger ranger;
		ranger = (Ranger) this.actorService.findByUserAccountId(LoginService.getPrincipal().getId());
		professionalRecords = ranger.getCurriculum().getProfessionalRecords();

		result = new ModelAndView("professional/ranger/list");
		result.addObject("professionalRecords", professionalRecords);

		return result;
	}

	@RequestMapping("/ranger/edit")
	public ModelAndView edit(@RequestParam int professionalId) {
		ModelAndView result;
		Professional professional;

		professional = professionalService.findOne(professionalId);

		Assert.notNull(professional);

		result = createEditModelAndView(professional);

		return result;
	}

	@RequestMapping(value = "/ranger/edit", method = RequestMethod.POST, params = "delete")
	public ModelAndView delete(final Professional professional, final BindingResult binding) {
		ModelAndView result;

		try {
			this.professionalService.delete(professional);
			result = new ModelAndView("redirect:list.do");
		} catch (final Throwable oops) {
			result = this.createEditModelAndView(professional, "professional.commit.error");
		}

		return result;
	}

	@RequestMapping(value = "/ranger/edit", method = RequestMethod.POST, params = "save")
	public ModelAndView save(@Valid Professional professional, BindingResult binding) {
		ModelAndView result;

		if (binding.hasErrors()) {
			result = createEditModelAndView(professional);
		} else {
			try {
				this.professionalService.save(professional);
				result = new ModelAndView("redirect:list.do");
			} catch (Throwable oops) {
				result = createEditModelAndView(professional, "professional.commit.error");
			}
		}

		return result;
	}

	@RequestMapping("/ranger/create")
	public ModelAndView create() {
		ModelAndView result;
		Professional professional;
		Ranger ranger;
		Curriculum curriculum;

		
		ranger = (Ranger) actorService.findByUserAccountId(LoginService.getPrincipal().getId());
		curriculum = ranger.getCurriculum();
		professional = professionalService.create(curriculum);

		result = createEditModelAndView(professional);

		return result;
	}

	private ModelAndView createEditModelAndView(Professional professional) {
		ModelAndView result;

		result = createEditModelAndView(professional, null);

		return result;
	}

	private ModelAndView createEditModelAndView(Professional professional, String messageCode) {
		ModelAndView result;

		result = new ModelAndView("professional/ranger/edit");

		result.addObject("professional", professional);
		result.addObject("message", messageCode);

		return result;
	}
}
