package controllers.story;

import java.util.Collection;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import controllers.AbstractController;

import security.LoginService;
import services.ActorService;
import services.StoryService;
import services.TripService;
import domain.Explorer;
import domain.Story;
import domain.Trip;

@Controller
@RequestMapping("/story")
public class StoryController extends AbstractController{
	
	@Autowired
	private StoryService storyService;
	@Autowired
	private ActorService actorService;
	@Autowired
	private TripService tripService;

	@RequestMapping("/list")
	public ModelAndView list(@RequestParam int tripId) {
		ModelAndView result;
		Collection<Story> stories;
		
		stories = storyService.getStoriesPerTrip(tripId);
		
		result = new ModelAndView("story/list");
		result.addObject("stories", stories);

		return result;
	}
	
	@RequestMapping("/explorer/list")
	public ModelAndView listMyStories() {
		ModelAndView result;
		Collection<Story> stories;
		Explorer explorer;
		
		explorer = (Explorer) actorService.findByUserAccountId(LoginService.getPrincipal().getId());
		stories = explorer.getStories();
		
		result = new ModelAndView("story/explorer/list");
		result.addObject("myStories",true);
		result.addObject("stories", stories);

		return result;
	}
	
	@RequestMapping(value = "/explorer/edit", method = RequestMethod.POST, params = "save")
	public ModelAndView save(@Valid Story story, BindingResult binding) {
		ModelAndView result;
		
		if(binding.hasErrors()){
			result = createEditModelAndView(story);
		}else{
			try{
				storyService.save(story);
				result = new ModelAndView("redirect:list.do");
			}catch(Throwable oops){
				result = createEditModelAndView(story, "story.commit.error");
			}
		}

		return result;
	}
	
	@RequestMapping("/explorer/edit")
	public ModelAndView edit(@RequestParam int storyId) {
		ModelAndView result;
		Story story;

		story = storyService.findOne(storyId);

		Assert.notNull(story);

		result = createEditModelAndView(story);

		return result;
	}
	
	@RequestMapping("/explorer/create")
	public ModelAndView create(@RequestParam int tripId) {
		ModelAndView result;
		Story story;
		Explorer explorer;
		Trip trip;

		trip = tripService.findOne(tripId);
		explorer = (Explorer) actorService.findByUserAccountId(LoginService.getPrincipal().getId());
		story = storyService.create(explorer, trip);
		
		result = createEditModelAndView(story);
		
		return result;
	}
	
	private ModelAndView createEditModelAndView(Story story) {
		ModelAndView result;

		result = createEditModelAndView(story, null);

		return result;
	}

	private ModelAndView createEditModelAndView(Story sponsorship, String messageCode) {
		ModelAndView result;

		result = new ModelAndView("story/explorer/edit");
		
		result.addObject("story", sponsorship);
		result.addObject("message", messageCode);

		return result;
	}

}
