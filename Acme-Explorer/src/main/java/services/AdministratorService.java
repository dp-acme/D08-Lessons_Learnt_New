
package services;

import java.util.ArrayList;
import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import repositories.AdministratorRepository;
import security.Authority;
import security.LoginService;
import security.UserAccount;
import domain.Actor;
import domain.Administrator;
import domain.Folder;
import domain.Manager;
import domain.Message;
import domain.Priority;
import domain.Ranger;
import domain.SocialIdentity;
import domain.Trip;

@Service
@Transactional
public class AdministratorService {

	// Managed repository --------------------------------------

	@Autowired
	private AdministratorRepository	administratorRepository;

	// Supporting services -------------------------------------

	@Autowired
	private MessageService			messageService;

	@Autowired
	private ActorService			actorService;

	@Autowired
	private ConfigurationService	configurationService;


	// Constructor -------------------------------------

	public AdministratorService() {
		super();
	}

	// CRUD methods ------------------------------------

	public Administrator create(final UserAccount userAccount) {
		Assert.notNull(userAccount);

		Administrator result;

		result = new Administrator();

		result.setBanned(false);
		result.setSuspicious(false);
		result.setFolders(new ArrayList<Folder>());
		result.setSocialIdentities(new ArrayList<SocialIdentity>());
		result.setUserAccount(userAccount);

		return result;
	}

	public Collection<Administrator> findAll() {
		Authority auth;
		UserAccount principal;
		Collection<Administrator> result;

		principal = LoginService.getPrincipal();
		auth = new Authority();
		auth.setAuthority(Authority.ADMINISTRATOR);

		Assert.isTrue(principal.getAuthorities().contains(auth));

		result = this.administratorRepository.findAll();

		return result;
	}

	public Administrator findOne(final int administratorId) {
		Assert.isTrue(administratorId != 0);

		Authority auth;
		UserAccount principal;
		Administrator result;

		principal = LoginService.getPrincipal();
		auth = new Authority();
		auth.setAuthority(Authority.ADMINISTRATOR);

		Assert.isTrue(principal.getAuthorities().contains(auth));

		result = this.administratorRepository.findOne(administratorId);

		return result;
	}

	public Administrator save(final Administrator administrator) {
		UserAccount principal;
		principal = LoginService.getPrincipal();
		Assert.isTrue(administrator.getUserAccount().equals(principal));

		Administrator result;

		result = this.administratorRepository.save(administrator);

		result.setSuspicious(this.configurationService.hasSpam(result));

		result = this.administratorRepository.save(result);

		return result;
	}

	// *****DASHBOARD*******

	Double fixToDefault(final Double input) {
		return input == null ? 0.0 : input;
	}

	Double[] fixToDefault(Double[] input) {
		if (input == null)
			input = new Double[] {};

		for (int i = 0; i < input.length; i++)
			input[i] = this.fixToDefault(input[i]);

		return input;
	}

	public Double[] getAvgMaxMinSdApplicationOfTrips() {
		Authority aut;
		UserAccount user;

		user = LoginService.getPrincipal();
		aut = new Authority();
		aut.setAuthority(Authority.ADMINISTRATOR);

		Assert.isTrue(user.getAuthorities().contains(aut));

		return this.fixToDefault(this.administratorRepository.getAvgMaxMinSdApplicationOfTrips());
	}

	public Double[] getAvgMaxMinSdManagersOfTrips() {
		Authority aut;
		UserAccount user;

		user = LoginService.getPrincipal();
		aut = new Authority();
		aut.setAuthority(Authority.ADMINISTRATOR);

		Assert.isTrue(user.getAuthorities().contains(aut));

		return this.fixToDefault(this.administratorRepository.getAvgMaxMinSdManagersOfTrips());
	}

	public Double[] getAvgMaxMinSdPriceOfTrips() {
		Authority aut;
		UserAccount user;

		user = LoginService.getPrincipal();
		aut = new Authority();
		aut.setAuthority(Authority.ADMINISTRATOR);

		Assert.isTrue(user.getAuthorities().contains(aut));

		return this.fixToDefault(this.administratorRepository.getAvgMaxMinSdPriceOfTrips());
	}

	public Double[] getAvgMaxMinSdRangersOfTrips() {
		Authority aut;
		UserAccount user;

		user = LoginService.getPrincipal();
		aut = new Authority();
		aut.setAuthority(Authority.ADMINISTRATOR);

		Assert.isTrue(user.getAuthorities().contains(aut));

		return this.fixToDefault(this.administratorRepository.getAvgMaxMinSdRangersOfTrips());
	}

	public Double getRatioOfPendingApplications() {
		Authority aut;
		UserAccount user;

		user = LoginService.getPrincipal();
		aut = new Authority();
		aut.setAuthority(Authority.ADMINISTRATOR);

		Assert.isTrue(user.getAuthorities().contains(aut));

		return this.fixToDefault(this.administratorRepository.getRatioOfPendingApplications());
	}

	public Double getRatioOfDueApplications() {
		Authority aut;
		UserAccount user;

		user = LoginService.getPrincipal();
		aut = new Authority();
		aut.setAuthority(Authority.ADMINISTRATOR);

		Assert.isTrue(user.getAuthorities().contains(aut));

		return this.fixToDefault(this.administratorRepository.getRatioOfDueApplications());
	}

	public Double getRatioOfAcceptedApplications() {
		Authority aut;
		UserAccount user;

		user = LoginService.getPrincipal();
		aut = new Authority();
		aut.setAuthority(Authority.ADMINISTRATOR);

		Assert.isTrue(user.getAuthorities().contains(aut));

		return this.fixToDefault(this.administratorRepository.getRatioOfAcceptedApplications());
	}

	public Double getRatioOfCancelledApplications() {
		Authority aut;
		UserAccount user;

		user = LoginService.getPrincipal();
		aut = new Authority();
		aut.setAuthority(Authority.ADMINISTRATOR);

		Assert.isTrue(user.getAuthorities().contains(aut));

		return this.fixToDefault(this.administratorRepository.getRatioOfCancelledApplications());
	}

	public Double getRatioOfCancelledTrips() {
		Authority aut;
		UserAccount user;

		user = LoginService.getPrincipal();
		aut = new Authority();
		aut.setAuthority(Authority.ADMINISTRATOR);

		Assert.isTrue(user.getAuthorities().contains(aut));

		return this.fixToDefault(this.administratorRepository.getRatioOfCancelledTrips());
	}

	public Collection<Trip> getTripsOver10PercentOverAvgApplications() {
		Authority aut;
		UserAccount user;

		user = LoginService.getPrincipal();
		aut = new Authority();
		aut.setAuthority(Authority.ADMINISTRATOR);

		Assert.isTrue(user.getAuthorities().contains(aut));

		return this.administratorRepository.getTripsOver10PercentOverAvgApplications();
	}

	public Collection<Object[]> getLegalTextNumberOfTrips() {
		Authority aut;
		UserAccount user;

		user = LoginService.getPrincipal();
		aut = new Authority();
		aut.setAuthority(Authority.ADMINISTRATOR);

		Assert.isTrue(user.getAuthorities().contains(aut));

		return this.administratorRepository.getLegalTextNumberOfTrips();
	}

	public Double[] getAvgMaxMinSdNotesOfTrips() {
		Authority aut;
		UserAccount user;

		user = LoginService.getPrincipal();
		aut = new Authority();
		aut.setAuthority(Authority.ADMINISTRATOR);

		Assert.isTrue(user.getAuthorities().contains(aut));

		return this.fixToDefault(this.administratorRepository.getAvgMaxMinSdNotesOfTrips());
	}

	public Double[] getAvgMaxMinSdAuditRecordsOfTrips() {
		Authority aut;
		UserAccount user;

		user = LoginService.getPrincipal();
		aut = new Authority();
		aut.setAuthority(Authority.ADMINISTRATOR);

		Assert.isTrue(user.getAuthorities().contains(aut));

		return this.fixToDefault(this.administratorRepository.getAvgMaxMinSdAuditRecordsOfTrips());
	}

	public Double getRatioOfTripsWithAuditRecords() {
		Authority aut;
		UserAccount user;

		user = LoginService.getPrincipal();
		aut = new Authority();
		aut.setAuthority(Authority.ADMINISTRATOR);

		Assert.isTrue(user.getAuthorities().contains(aut));

		return this.fixToDefault(this.administratorRepository.getRatioOfTripsWithAuditRecords());
	}

	public Double getRatioOfRangersWithCurriculum() {
		Authority aut;
		UserAccount user;

		user = LoginService.getPrincipal();
		aut = new Authority();
		aut.setAuthority(Authority.ADMINISTRATOR);

		Assert.isTrue(user.getAuthorities().contains(aut));

		return this.fixToDefault(this.administratorRepository.getRatioOfRangersWithCurriculum());
	}

	public Double getRatioOfRangersWithEndorsedCurriculum() {
		Authority aut;
		UserAccount user;

		user = LoginService.getPrincipal();
		aut = new Authority();
		aut.setAuthority(Authority.ADMINISTRATOR);

		Assert.isTrue(user.getAuthorities().contains(aut));

		return this.fixToDefault(this.administratorRepository.getRatioOfRangersWithEndorsedCurriculum());
	}

	public Double getRatioOfSuspiciousManagers() {
		Authority aut;
		UserAccount user;

		user = LoginService.getPrincipal();
		aut = new Authority();
		aut.setAuthority(Authority.ADMINISTRATOR);

		Assert.isTrue(user.getAuthorities().contains(aut));

		return this.fixToDefault(this.administratorRepository.getRatioOfSuspiciousManagers());
	}

	public Double getRatioOfSuspiciousRangers() {
		Authority aut;
		UserAccount user;

		user = LoginService.getPrincipal();
		aut = new Authority();
		aut.setAuthority(Authority.ADMINISTRATOR);

		Assert.isTrue(user.getAuthorities().contains(aut));

		return this.fixToDefault(this.administratorRepository.getRatioOfSuspiciousRangers());
	}

	public Message broadcastNotification(final String subject, final String body, final Priority priority, final Actor actor) {
		Assert.hasLength(subject);
		Assert.hasLength(body);
		Assert.notNull(actor);
		Assert.notNull(priority);

		Authority aut;
		Message result;

		aut = new Authority();
		aut.setAuthority(Authority.ADMINISTRATOR);

		Assert.isTrue(actor.getUserAccount().getAuthorities().contains(aut));

		result = this.messageService.create(actor);
		result.setRecipients(this.actorService.findAll());
		result.setSubject(subject);
		result.setBody(body);
		result.setPriority(priority);

		result = this.messageService.sendMessage(result, true);

		return result;
	}

	public void banActor(final int actorId) {
		UserAccount principal;
		Authority auth;

		principal = LoginService.getPrincipal();
		auth = new Authority();
		auth.setAuthority(Authority.ADMINISTRATOR);

		Assert.isTrue(principal.getAuthorities().contains(auth));

		Actor actor;

		actor = this.actorService.findOne(actorId);

		Assert.notNull(actor);
		Assert.isTrue(actor.isSuspicious());

		actor.getUserAccount().setAccountNonLocked(false);

		this.actorService.save(actor);
	}

	public void unbanActor(final int actorId) {
		UserAccount principal;
		Authority auth;

		principal = LoginService.getPrincipal();
		auth = new Authority();
		auth.setAuthority(Authority.ADMINISTRATOR);

		Assert.isTrue(principal.getAuthorities().contains(auth));

		Actor actor;

		actor = this.actorService.findOne(actorId);

		Assert.notNull(actor);
		Assert.isTrue(actor.isSuspicious());

		actor.getUserAccount().setAccountNonLocked(true);

		this.actorService.save(actor);
	}

	public Collection<Ranger> getSuspiciousRangers() {
		Collection<Ranger> result;
		UserAccount principal;
		Authority auth;

		principal = LoginService.getPrincipal();
		auth = new Authority();
		auth.setAuthority(Authority.ADMINISTRATOR);

		Assert.isTrue(principal.getAuthorities().contains(auth));

		result = this.administratorRepository.getSuspiciousRangers();

		return result;
	}

	public Collection<Manager> getSuspiciousManagers() {
		Collection<Manager> result;
		UserAccount principal;
		Authority auth;

		principal = LoginService.getPrincipal();
		auth = new Authority();
		auth.setAuthority(Authority.ADMINISTRATOR);

		Assert.isTrue(principal.getAuthorities().contains(auth));

		result = this.administratorRepository.getSuspiciousManagers();

		return result;
	}

	public Collection<Actor> getSuspiciousActors() {
		Collection<Actor> result;
		UserAccount principal;
		Authority auth;

		principal = LoginService.getPrincipal();
		auth = new Authority();
		auth.setAuthority(Authority.ADMINISTRATOR);

		Assert.isTrue(principal.getAuthorities().contains(auth));

		result = this.administratorRepository.getSuspiciousActors();

		return result;
	}
}
