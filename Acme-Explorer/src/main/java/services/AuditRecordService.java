package services;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import repositories.AuditRecordRepository;
import security.Authority;
import security.LoginService;
import security.UserAccount;
import domain.AuditRecord;
import domain.Auditor;
import domain.Trip;

@Service
@Transactional
public class AuditRecordService {
	// Managed repository --------------------------------------

	@Autowired
	private AuditRecordRepository auditRecordRepository;

	// Supporting services -------------------------------------

	@Autowired
	private AuditorService auditorService;
	

	@Autowired
	private ActorService actorService;

	@Autowired
	private TripService tripService;
	
	// Constructors --------------------------------------------

	public AuditRecordService() {
		super();
	}
	// Simple CRUD methods -------------------------------------

	
	public AuditRecord create(int tripId) {
		Auditor auditor;
		Trip trip;
		UserAccount principal;
	
		Authority auth;
		auth = new Authority();
		auth.setAuthority(Authority.AUDITOR);
		principal = LoginService.getPrincipal();
		Assert.isTrue(principal.getAuthorities().contains(auth));

		auditor = (Auditor) actorService.findByUserAccountId(principal.getId());
		trip  = tripService.findOne(tripId);
		
		return create(auditor,trip);
	}
	
	public AuditRecord create(Auditor auditor, Trip trip) {
		Assert.notNull(auditor);
		Assert.notNull(trip);

		AuditRecord result;

		result = new AuditRecord();
		result.setTime(new Date(System.currentTimeMillis() - 1));
		result.setAttachments(new ArrayList<String>());
		result.setDraft(true);
		result.setAuditor(auditor);
		result.setTrip(trip);

		return result;
	}

	public Collection<AuditRecord> findAll() {
		Collection<AuditRecord> result;

		result = auditRecordRepository.findAll();

		return result;
	}

	public AuditRecord findOne(int auditRecordId) {
		Assert.isTrue(auditRecordId != 0);

		AuditRecord result;

		result = auditRecordRepository.findOne(auditRecordId);

		return result;
	}

	private static boolean urlCheck(String str){
		boolean res = true;
		
		try{
			new URL(str);
		} catch (MalformedURLException e) {
			res = false;
		}
		
		return res;
	}
	
	public AuditRecord save(AuditRecord auditRecord) {
		Assert.notNull(auditRecord);
		Assert.isTrue(auditRecord.getTrip().getPublicationDate().before(new Date()));


		AuditRecord result, auditRecordOld;
		UserAccount principal;
	
		Authority auth;

		auth = new Authority();
		auth.setAuthority(Authority.AUDITOR);
		principal = LoginService.getPrincipal();
		
		Assert.isTrue(principal.getAuthorities().contains(auth));

		for(String str : auditRecord.getAttachments()){
			Assert.isTrue(urlCheck(str));
		}
		
		if (auditRecord.getId() != 0) {
			Assert.isTrue(auditRecord.getAuditor().getUserAccount()
					.equals(principal));
			auditRecordOld = findOne(auditRecord.getId());
			Assert.isTrue(auditRecordOld.isDraft());
		}

		result = auditRecordRepository.save(auditRecord);

		if (auditRecord.getId() == 0) {
			Auditor auditor;

			auditor = result.getAuditor();
			auditor.getAuditRecords().add(result);
			auditorService.save(auditor);
		}

		return result;
	}

	public void delete(AuditRecord auditRecord) {
		Assert.notNull(auditRecord);

		UserAccount principal;
		Auditor auditor;

		principal = LoginService.getPrincipal();
		auditor = auditRecord.getAuditor();

		Assert.isTrue(auditRecord.isDraft());
		Assert.isTrue(auditRecord.getAuditor().getUserAccount()
				.equals(principal));
		Assert.isTrue(auditRecordRepository.exists(auditRecord.getId()));

		auditor.getAuditRecords().remove(auditRecord);

		auditorService.save(auditor);

		auditRecordRepository.delete(auditRecord);
	}
	
	
	
	
	public Collection<AuditRecord> findAllByPrincipal() {
		Collection<AuditRecord> result;
		UserAccount principal;
		
		principal = LoginService.getPrincipal();

		result = auditRecordRepository.findAllByPrincipal(principal.getId());

		return result;
	}
	public Collection<AuditRecord> findAllByTrip(int tripId){
		Collection<AuditRecord> result;
		
			result = auditRecordRepository.findAllDraftedByTrip(tripId);	
			Assert.notNull(result);
			return result;
	}


	public Collection<AuditRecord> findAllByTripAuditor(int tripId) {
		Collection<AuditRecord> result;
		UserAccount principal;
		
		principal= LoginService.getPrincipal();
		
		result = auditRecordRepository.findAllByTripAndAuditor(tripId,principal.getId());

		return result;
	}
	
	

}
