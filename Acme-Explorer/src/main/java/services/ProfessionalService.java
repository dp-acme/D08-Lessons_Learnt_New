
package services;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import repositories.ProfessionalRepository;
import security.LoginService;
import security.UserAccount;
import domain.Curriculum;
import domain.Professional;

@Service
@Transactional
public class ProfessionalService {

	// Managed repository --------------------------------------
	
	@Autowired
	private ProfessionalRepository	professionalRepository;

	// Supporting services -------------------------------------

	@Autowired
	private CurriculumService curriculumService;
	
	// Constructors --------------------------------------------
	
	public ProfessionalService() {
		super();
	}
	
	// Simple CRUD methods -------------------------------------

	public Professional create(Curriculum curriculum) {
		UserAccount principal;
		Professional result;
		
		principal = LoginService.getPrincipal();
		Assert.isTrue(curriculum.getRanger().getUserAccount().equals(principal));
		
		result = new Professional();
		result.setCurriculum(curriculum);
		
		return result;
	}

	public Collection<Professional> findAll() {
		Collection<Professional> result;

		result = this.professionalRepository.findAll();
		Assert.notNull(result);

		return result;
	}
	
	public Professional findOne(int professionalId) {
		Assert.isTrue(professionalId != 0);
		
		Professional result;

		result = this.professionalRepository.findOne(professionalId);

		return result;
	}

	public Professional save(Professional professional) {
		Assert.notNull(professional);
		
		UserAccount principal;
		Professional result;
		Curriculum curriculum;
		
		curriculum = professional.getCurriculum();
		principal = LoginService.getPrincipal();
		Assert.isTrue(professional.getCurriculum().getRanger().getUserAccount().equals(principal));
		
		if (professional.getProfessionalEnd() != null){
			Assert.isTrue(professional.getProfessionalStart().before(professional.getProfessionalEnd()));			
		}

		result = professionalRepository.save(professional);

		if (professional.getId() == 0) {
			curriculum.getProfessionalRecords().add(result);
			curriculumService.save(curriculum);
		}
				
		return result;
	}

	public void delete(Professional professional) {
		Assert.notNull(professional);
		
		UserAccount principal;
		Curriculum curriculum;
		
		curriculum = professional.getCurriculum();
		principal = LoginService.getPrincipal();
		Assert.isTrue(curriculum.getRanger().getUserAccount().equals(principal));
		Assert.isTrue(professionalRepository.exists(professional.getId()));
		
		curriculum.getProfessionalRecords().remove(professional);
		curriculumService.save(curriculum);
		
		professionalRepository.delete(professional);
	}
	
	//Other business methods -----------------------------------
	
	public void deleteAllRecordsOfCurriculum(Curriculum c){
		UserAccount principal;
		
		principal = LoginService.getPrincipal();
		Assert.isTrue(c.getRanger().getUserAccount().equals(principal));

		professionalRepository.deleteInBatch(c.getProfessionalRecords());
	}
}
