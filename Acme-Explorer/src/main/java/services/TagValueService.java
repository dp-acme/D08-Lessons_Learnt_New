package services;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import repositories.TagValueRepository;
import security.Authority;
import security.LoginService;
import security.UserAccount;
import domain.TagKey;
import domain.TagValue;
import domain.Trip;

@Service
@Transactional
public class TagValueService {
	
	// Managed repository --------------------------------------
	
	@Autowired
	private TagValueRepository tagValueRepository;
	
	// Supporting services -------------------------------------
	
	@Autowired
	private TripService tripService;
	
	// Constructors --------------------------------------------
	
	public TagValueService(){
		super();
	}
	
	// Simple CRUD methods -------------------------------------
	
	public TagValue create() {
//		Assert.notNull(tagKey);
		
		TagValue result;
		
		result = new TagValue();
//		result.setTagKey(tagKey);
		
		return result;
	}
	
	public Collection<TagValue> findAll() {
		Collection<TagValue> result;
		
		result = tagValueRepository.findAll();
		
		return result;
	}

	public TagValue findOne(int tagValueId) {
		Assert.isTrue(tagValueId!=0);
		
		TagValue result;
		
		result = tagValueRepository.findOne(tagValueId);
		
		return result;
	}
	
	public TagValue save(TagValue tagValue){
		Assert.notNull(tagValue);	

		UserAccount principal;
		Authority auth;
		TagValue result, databaseTagValue;

		principal = LoginService.getPrincipal();
		auth = new Authority();
		auth.setAuthority(Authority.MANAGER);
		
		Assert.isTrue(principal.getAuthorities().contains(auth));
		
		if(tagValue.getId() != 0){ //No se puede editar el valor
			databaseTagValue = tagValueRepository.findOne(tagValue.getId());
			
			Assert.isTrue(tagValue.getValue().equals(databaseTagValue.getValue()));
		}
				
		result = tagValueRepository.save(tagValue);
		
		return result;
	}
	
	public void delete(TagValue tagValue){
		Assert.notNull(tagValue);
		Assert.isTrue(tagValueRepository.exists(tagValue.getId()));
		
		Collection<Trip> trips;
		java.util.Iterator<Trip> tripIterator;

		trips = tripService.findAll();
		
		tripIterator = trips.iterator();
		while(tripIterator.hasNext()){
			Trip t = tripIterator.next();
			tripIterator.remove();
			
			if(t.getTags().contains(tagValue)){
				tripService.eraseTagValueFromTrip(t, tagValue);
			}
		}
		
		tagValueRepository.delete(tagValue);
	}
	
	public void deleteInBatch(Collection<TagValue> tagValues){
		Assert.notNull(tagValues);
		
		Collection<Trip> trips;
		java.util.Iterator<Trip> tripIterator;
		java.util.Iterator<TagValue> tagIterator;
		
		trips = tripService.findAll();
		
		tagIterator = tagValues.iterator();
		while(tagIterator.hasNext()){
			TagValue tv = tagIterator.next();
			tagIterator.remove();
			
			tripIterator = trips.iterator();
			while(tripIterator.hasNext()){
				Trip t = tripIterator.next();
				tripIterator.remove();
				
				if(t.getTags().contains(tv)){					
					tripService.eraseTagValueFromTrip(t, tv);
				}
			}	
		}
				
		tagValueRepository.deleteInBatch(tagValues);
	}

	public Collection<TagValue> filterByTagKey(TagKey tagKey) {
		Collection<TagValue> result;
		
		result = tagValueRepository.filterByTagKey(tagKey);
		
		return result;
	}

	public int getTagValueReferences(TagValue tagValue) {
		Integer result;
		
		result = tagValueRepository.getTagValueReferences(tagValue.getId());
		
		return result;
	}

}
