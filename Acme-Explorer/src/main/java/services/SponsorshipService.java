package services;

import java.util.Collection;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import repositories.SponsorshipRepository;
import security.LoginService;
import security.UserAccount;
import domain.Sponsor;
import domain.Sponsorship;
import domain.Trip;

@Service
@Transactional
public class SponsorshipService {
	
	// Managed repository ---------------------------------------------------
	
	@Autowired
	private SponsorshipRepository sponsorshipRepository;
	
	// Supporting services ---------------------------------------------------

	@Autowired
	private SponsorService sponsorService;
	
	// Constructor ---------------------------------------------------
	
	public SponsorshipService(){
		super();
	}
	
	// Simple CRUD methods ---------------------------------------------------
	
	public Sponsorship create(Sponsor sponsor, Trip trip) {
		Sponsorship result;
		
		result = new Sponsorship();
		result.setSponsor(sponsor);
		result.setTrip(trip);
		
		return result;
	}
	
	public Collection<Sponsorship> findAll() {
		Collection<Sponsorship> result;
		
		result = sponsorshipRepository.findAll();
		Assert.notNull(result);

		return result;
	}

	public Sponsorship findOne(int sponsorshipId) {
		Assert.isTrue(sponsorshipId != 0);
		
		Sponsorship result;
		
		result = sponsorshipRepository.findOne(sponsorshipId);
		
		return result;
	}
	
	public Sponsorship save(Sponsorship sponsorship){
		Assert.notNull(sponsorship);
		Assert.isNull(sponsorship.getTrip().getCancellationReason());
		Assert.isTrue(sponsorship.getTrip().getPublicationDate().before(new Date()));
		//Assert.isTrue(sponsorship.getTrip().getStartDate().after(new Date()));

		Sponsorship result;
		UserAccount principal;
		
		principal = LoginService.getPrincipal();
		Assert.isTrue(sponsorship.getSponsor().getUserAccount().equals(principal));
		
		result = sponsorshipRepository.save(sponsorship);
		
		if (sponsorship.getId() == 0) {
			Sponsor sponsor;
			
			sponsor = sponsorship.getSponsor();
			sponsor.getSponsorships().add(result);
			
			sponsorService.save(sponsor);
		}
		
		Assert.notNull(result.getCreditCard().getBrandName());
		Assert.notNull(result.getCreditCard().getHolderName());
		Assert.notNull(result.getCreditCard().getExpirationMonth());
		Assert.notNull(result.getCreditCard().getExpirationYear());
		Assert.notNull(result.getCreditCard().getNumber());
		
		return result;
	}
	
	// Other business methods ---------------------------------------------------
	
	public Collection<Sponsorship> getSponsorshipsByTrip(int tripId){
		Collection<Sponsorship> result;
		
		result = sponsorshipRepository.getSponsorshipsByTrip(tripId);
		Assert.notNull(result);

		return result;
	}
	
	
}
