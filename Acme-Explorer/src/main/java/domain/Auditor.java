package domain;

import java.util.Collection;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

@Entity
@Access(AccessType.PROPERTY)
public class Auditor extends Actor {

	// Constructor
	
	public Auditor() {
		super();
	}
	
	// Relationships
	
	private Collection<Note> notes;
	private Collection<AuditRecord> auditRecords;
	
	@NotNull
	@Valid
	@OneToMany(mappedBy = "auditor")
	public Collection<Note> getNotes() {
		return notes;
	}
	public void setNotes(Collection<Note> notes) {
		this.notes = notes;
	}
	
	@NotNull
	@Valid
	@OneToMany(mappedBy = "auditor")
	public Collection<AuditRecord> getAuditRecords() {
		return auditRecords;
	}
	public void setAuditRecords(Collection<AuditRecord> auditRecords) {
		this.auditRecords = auditRecords;
	}
	
}
