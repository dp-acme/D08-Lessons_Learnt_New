package domain;

import java.util.Date;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;

import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Access(AccessType.PROPERTY)
public class ApplyFor extends DomainEntity {

	// Constructor
	
	public ApplyFor() {
		super();
	}
	
	// Attributes
	
	private Date moment;
	private Status status;
	private String comments;
	private String rejectedReason;
	private CreditCard creditCard;
	
	@Past
	@NotNull
	@DateTimeFormat(pattern = "dd/MM/yyyy HH:mm") // mm/dd/yyyy HH:MM
	@Temporal(TemporalType.TIMESTAMP)
	public Date getMoment() {
		return moment;
	}
	public void setMoment(Date moment) {
		this.moment = moment;
	}
	
	@NotNull
	@Valid
	public Status getStatus() {
		return status;
	}
	public void setStatus(Status status) {
		this.status = status;
	}
	
	public String getComments() {
		return comments;
	}
	public void setComments(String comments) {
		this.comments = comments;
	}
	
	public String getRejectedReason() {
		return rejectedReason;
	}
	public void setRejectedReason(String rejectedReason) {
		this.rejectedReason = rejectedReason;
	}
	
	@Valid
	public CreditCard getCreditCard() {
		return creditCard;
	}
	public void setCreditCard(CreditCard creditCard) {
		this.creditCard = creditCard;
	}
	
	
	// Relationships
	
	private Explorer explorer;
	private Trip trip;

	@NotNull
	@Valid
	@ManyToOne(optional=false)
	public Explorer getExplorer() {
		return explorer;
	}
	public void setExplorer(Explorer explorer) {
		this.explorer = explorer;
	}
	
	@NotNull
	@Valid
	@ManyToOne(optional=false)
	public Trip getTrip() {
		return trip;
	}
	public void setTrip(Trip trip) {
		this.trip = trip;
	}
	
}
