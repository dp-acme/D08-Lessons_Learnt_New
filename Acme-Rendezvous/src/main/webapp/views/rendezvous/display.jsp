<%--
 * action-1.jsp
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page import="org.springframework.security.core.GrantedAuthority"%>
<%@page import="security.Authority"%>
<%@page import="security.UserAccount"%>

<%@page
	import="org.springframework.security.core.context.SecurityContextHolder"%>
<%@page import="security.LoginService"%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<%@taglib prefix="acme" tagdir="/WEB-INF/tags"%>


<jstl:set var="requestURI" value="rendezvous/display.do" />

<jstl:if test="${not empty rendezvous.getPicture()}" >
	<img src="${rendezvous.picture}" width="300" />
	<br />
	<br />
</jstl:if>

<!-- Name -->
<h2>
	<jstl:out value="${rendezvous.name}" />
</h2>

<jstl:if test="${rendezvous.isDeleted()}">
	<h3>
		[<spring:message code="rendezvous.display.isDeleted" />]
	</h3>
</jstl:if>

<!-- BOTONES/ENLACES (UNIRME/DESUNIRME/ EDITAR/ BORRAR(ADMIN)) -->

<% pageContext.setAttribute("date", new Date()); %>

<security:authorize access="hasRole('USER')">
	<security:authentication property="principal" var="principal" />

	<jstl:if test="${user != null && !rendezvous.getUsers().contains(user) && rendezvous.getCreator() != user && 
		!rendezvous.isDraft() && !rendezvous.isDeleted() && (!rendezvous.isAdultsOnly() || isAdult) &&
		date.before(rendezvous.getMoment())}">
		<a href="answer/user/edit.do?rendezvousId=${param.rendezvousId}">
			<spring:message code="rendezvous.display.rsvp" />
		</a>
		<br />
		<br />
	</jstl:if>
	
	<jstl:if test="${user != null && rendezvous.getUsers().contains(user) && rendezvous.getCreator() != user && 
		!rendezvous.isDraft() && !rendezvous.isDeleted() && (!rendezvous.isAdultsOnly() || isAdult) &&
		date.before(rendezvous.getMoment())}">
		<a href="answer/user/delete.do?rendezvousId=${param.rendezvousId}" 
			onClick="javascript: return confirm('<spring:message code="rendezvous.display.ConfirmCancelRSVP" />');">
			<spring:message code="rendezvous.display.cancelRsvp" />
		</a>
		<br />
		<br />
	</jstl:if>

	<jstl:if test="${principal == rendezvous.getCreator().getUserAccount() && rendezvous.isDraft() && !rendezvous.isDeleted() &&
		date.before(rendezvous.getMoment())}">
		<a href="rendezvous/user/edit.do?rendezvousId=${param.rendezvousId}">
			<spring:message code="rendezvous.display.editRendezvous" />
		</a>
		<br />
		<br />
	</jstl:if>
</security:authorize>

<security:authorize access="hasRole('ADMIN')">
	<jstl:if test="${!rendezvous.isDraft() && !rendezvous.isDeleted() }">
		<a href="rendezvous/admin/delete.do?rendezvousId=${param.rendezvousId}" 
			onClick="javascript: return confirm('<spring:message code="rendezvous.display.ConfirmDeleteRendezvous" />');">
			<spring:message code="rendezvous.display.delete" />
		</a>
		<br />
		<br />
	</jstl:if>
</security:authorize>

<!-- Description -->
<spring:message code="rendezvous.display.description" />:
		<jstl:out value="${rendezvous.description}" />

<br />
<br />

<!-- Moment -->
<spring:message var="formatDatePattern" code="rendezvous.display.formatDatePattern" />
<spring:message code="rendezvous.display.moment" />:
		<fmt:formatDate value="${rendezvous.moment}" pattern="${formatDatePattern}" />

<br />
<br />

<jstl:if test="${rendezvous.isAdultsOnly()}">
	<b>
		<spring:message code="rendezvous.display.isAdultsOnly" />
	</b>
	
	<br />
	<br />
</jstl:if>

<!-- Latitude -->
<jstl:if test="${rendezvous.getLatitude() != null}" >
	<spring:message code="rendezvous.display.latitude" />:
			<jstl:out value="${rendezvous.latitude}" />	
&nbsp;&nbsp;
</jstl:if>
<!-- Longitude -->
<jstl:if test="${rendezvous.getLongitude() != null}" >
<spring:message code="rendezvous.display.longitude" />:
		<jstl:out value="${rendezvous.longitude}" />
		
<br />
<br />
</jstl:if>

<a href="question/list.do?rendezvousId=${rendezvous.getId()}">
	<spring:message code="rendezvous.display.list.question" />
</a>

<br />
<br />

<a href="announcement/list.do?rendezvousId=${rendezvous.getId()}">
	<spring:message code="rendezvous.display.list.announcement" />
</a>

<br />
<br />
	
<h3>
	<spring:message code="rendezvous.display.links" />:
</h3>
<jstl:set var="showingSimilar" value="false" />
<jstl:forEach var="i" items="${rendezvous.getLinks()}">
	<security:authorize access="hasRole('ADMIN')">
		<a href="rendezvous/display.do?rendezvousId=${i.getId()}">
			<jstl:out value="${i.getName()}" />
		</a>
		<br />
	</security:authorize>
	<security:authorize access="hasRole('USER') || isAnonymous()">
		<jstl:if test="${!i.isAdultsOnly() || (i.isAdultsOnly() && isAdult != null && isAdult)}">
			<a href="rendezvous/display.do?rendezvousId=${i.getId()}">
				<jstl:out value="${i.getName()}" />
			</a>
			<br />
			<jstl:set var="showingSimilar" value="true" />
		</jstl:if>
	</security:authorize>
</jstl:forEach>
<jstl:if test="${empty rendezvous.getLinks() || !showingSimilar}">
	<spring:message code="rendezvous.display.messageToEmptyLinks" />
</jstl:if>

<security:authorize access="hasRole('USER')">
	<br />
	<security:authentication property="principal" var="principal" />
	<jstl:if test="${rendezvous.getCreator().getUserAccount().equals(principal) && !rendezvous.isDeleted()}">
		<acme:cancel url="rendezvous/user/linkSimilars.do?rendezvousId=${rendezvous.getId()}" code="rendezvous.display.linkSimilarRendezvouses"/>
	</jstl:if>
</security:authorize>

<br />

<h3>
	<spring:message code="rendezvous.display.ListUsers"/>
</h3>
<display:table name="users" id="row" requestURI="${requestURI}" pagesize="5">

	<display:column property="userAccount.username" titleKey="rendezvous.display.user.username" sortable="true" />

	<display:column property="name" titleKey="rendezvous.display.user.name" sortable="true" />

	<display:column property="name" titleKey="rendezvous.display.user.surname" sortable="true" />

	<display:column title="" sortable="false">
		<spring:url value="user/display.do?userId=${row.getId()}" var="displayUser" />
		<a href="${displayUser}">
			<spring:message code="rendezvous.display.user.profile" />
		</a>
	</display:column>
	
</display:table>

<br />

<!-- CREAR COMENTARIOS -->
<h3>
	<spring:message code="rendezvous.display.ListComments" />
</h3>
<security:authorize access="isAuthenticated()"> 
	<security:authentication property="principal" var="principal" />
</security:authorize>

<security:authorize access="hasRole('USER')">
	<jstl:if test="${joined}">
		<fieldset style="display: inline;"> 
			<form:form modelAttribute="newComment" action="comment/user/edit.do?rendezvousId=${rendezvous.id}"> 
				<form:hidden path="id"/>
				<form:hidden path="version"/>
				<form:hidden path="deleted"/>
				<form:hidden path="moment"/>
				
				<acme:textarea path="text" code="comment.create.text"/>
				<acme:textbox path="picture" code="comment.create.picture"/>
			
				<acme:submit name="save" code="comment.create.createButton"/>
			</form:form>
		</fieldset> 
	</jstl:if>
</security:authorize>

<!-- RESPONDER COMENTARIOS -->

<div style="display: none;" id="replyForm">
<security:authorize access="hasRole('USER')">
	<p id = "replyFormIndentation" style="float: left;"></p>
	
	<fieldset style="display: inline;"> 
		<form:form modelAttribute="newReply" action=""> 
		<form:hidden path="id"/>
		<form:hidden path="version"/>
		<form:hidden path="deleted"/>
		<form:hidden path="moment"/>
		
		<acme:textarea path="text" code="comment.create.text"/>
		<acme:textbox path="picture" code="comment.create.picture"/>
	
		<acme:submit name="reply" code="comment.create.createButton"/>
		</form:form>
	</fieldset>

</security:authorize>
</div>

<script>
function replyFormIn(tableId, rendezvousId, parentId, indentation){
	document.getElementById('newReply').action = "comment/user/edit.do?rendezvousId=" + rendezvousId + "&parentId=" + parentId;
	document.getElementById('replyFormIndentation').innerHTML = indentation;
	document.getElementById(tableId).appendChild(document.getElementById('replyForm'));
	document.getElementById('replyForm').style.display = 'inline';
}

function confirmAndDelete(commentId, rendezvouzId, message){
	if(confirm(message)){
		relativeRedir('comment/admin/delete.do?rendezvousId=' + rendezvouzId + "&commentId=" + commentId);	
	}
}
</script>

<!-- MOSTRAR COMENTARIOS -->

<spring:message code="comment.table.user" var="userHeader"/>
<spring:message code="comment.table.text" var="textHeader"/>
<spring:message code="comment.table.moment" var="momentHeader"/>

<spring:message code="comment.dateFormat" var="dateF"/>

<spring:message code="comment.deletedMessage" var="deletedMessage"/>
<spring:message code="comment.deletionMessage" var="deletionMessage"/>

<spring:message code="comment.table.replyButton" var="replyButton"/>
<spring:message code="comment.table.deleteButton" var="deleteButton"/>

<jstl:set var="cs" value="${comments}" />

<%@ page import="org.springframework.web.util.HtmlUtils" %> 
<%@ page import="org.springframework.security.core.authority.SimpleGrantedAuthority" %>
<%@ page import="java.util.Collection" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.Map" %>
<%@ page import="java.util.HashMap" %>
<%@ page import="java.util.Date" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="domain.Comment" %>

<%  
	@SuppressWarnings("unchecked")
	Collection<Comment> javaComments = (Collection<Comment>)pageContext.getAttribute("cs"); 
	Map<Integer, Integer> indentationMap = new HashMap<Integer, Integer>();
	String[] headers = {(String)pageContext.getAttribute("userHeader"), (String)pageContext.getAttribute("textHeader"), (String)pageContext.getAttribute("momentHeader")};
	
	String dateFormat = (String)pageContext.getAttribute("dateF");
	String deleteMessage = (String)pageContext.getAttribute("deletedMessage");
	String deletionMessage = (String)pageContext.getAttribute("deletionMessage");
	String deleteButton = (String)pageContext.getAttribute("deleteButton");
	String replyButton = (String)pageContext.getAttribute("replyButton");

	UserAccount user = (UserAccount)pageContext.getAttribute("principal");
%>

<%! 
	String safeHTML(String str){ //Funci�n para hacer HTML seguro
		return HtmlUtils.htmlEscape(str);
	}
%>

<%! 
	String formatDate(Date date, String format){ //Funci�n para formatear fechas
		SimpleDateFormat dt1 = new SimpleDateFormat(format);
		
		return dt1.format(date);
	}
%>

<%! 
	String indent(Integer tabs){ //Funci�n para indentar niveles
		String res = "";
		
		for(int i = 0; i < tabs; i ++){
			res += "&emsp;&emsp;&emsp;";
		}
		
		return res;
	}
%>

<%!
	String printComment(Integer indentation, Comment comment, String[] headers, String dateFormat, String deletedMessage, 
						Map<Integer, Integer> indentationMap, String authority, String deletionMessage,
						String deleteButton, String replyButton, UserAccount user){
		String res = "";
		
		indentationMap.put(comment.getId(), indentation);
		
		res += "<div id = comment" + comment.getId() + ">";		
		
		res += "<p style = 'float: left;'>" + indent(indentation) + "</p>";
		
		res += "<table border = '1'>";

		res +=  "<tr style='background-color: yellow;'>" +
				"<th>" + safeHTML(headers[0]) + "</th>" +
				"<th>" + safeHTML(headers[1]) + "</th>" +
				"<th>" + safeHTML(headers[2]) + "</th>" +
				"</tr>";
			
		res +=  "<tr>";
		
		res +=	"<td>" + safeHTML(comment.getUser().getUserAccount().getUsername()) + "</td>";

		if(!comment.getDeleted()){ //Si el comentario no est� borrado se muestra el texto y la imagen (si la tiene)
			res +=	"<td>" + safeHTML(comment.getText()) + "</td>";
			res += "<td>" + safeHTML(formatDate(comment.getMoment(), dateFormat)) + "</td>";
					
			if(comment.getPicture() != null && !comment.getPicture().equals("")){
				res += 	"<td><img src = '" + safeHTML(comment.getPicture()) + "' width='150' height='150'></img></td>";			
			}
			
		} else{ //Si lo est� se muestra un mensaje gen�rico internacionalizado
			res += 	"<td><b>" + safeHTML(deletedMessage) + "</b></td>";
			res += "<td>" + safeHTML(formatDate(comment.getMoment(), dateFormat)) + "</td>";
		}

		res += "</tr>";

		boolean joined = comment.getRendezvous().getCreator().getUserAccount().equals(user);
		
		if(!joined){
			for(domain.User u : comment.getRendezvous().getUsers()){
				if(u.getUserAccount().equals(user)){
					joined = true;
					break;
				}
			}
		}
		
		if(authority.equals("USER") && joined){ //Si se trata de un usuario
			res += "<tr>";
			res += "<td>";
			res += "<button onclick=\"javascript: replyFormIn('comment" + comment.getId() + "', " + 
					comment.getRendezvous().getId() + ", " + 
					comment.getId() + ", " + 
					"'" + indent(indentation) + "');\"" + 
					">" + safeHTML(replyButton) + "</button>";
			res += "</td>";
			res += "</tr>";	
			
		} else if(authority.equals("ADMIN") && !comment.getDeleted()){ //Si se trata de un admin
			res += "<tr>";
			res += "<td>";
			res += "<button onclick=\"javascript: confirmAndDelete(" + comment.getId() + ", " +
				   comment.getRendezvous().getId() + ", " +
				   "'" + safeHTML(deletionMessage) + "'" +
				   ");\">" + safeHTML(deleteButton) + "</button>";
			res += "</td>";
			res += "</tr>";	
		}

		res += "</table>";
		
		res += "</div>";
		
		return res;
	}
%>

<%! 
	String printCommentList(Integer indentation, Collection<Comment> comments, String[] headers, String dateFormat, String deletedMessage, 
							Map<Integer, Integer> indentationMap, String authority, String deletionMessage,
							String deleteButton, String replyButton, UserAccount user){
		String res = "";
		
		for(Comment c : comments){
			res += printComment(indentation, c, headers, dateFormat, deletedMessage, indentationMap, authority, deletionMessage, deleteButton, replyButton, user);
			
			if(c.getReplies().size() > 0){ //Si el comentario tiene respuestas, estas se muestran con una indentaci�n superior
				res += printCommentList(indentation + 1, c.getReplies(), headers, dateFormat, deletedMessage, indentationMap, 
						authority, deletionMessage, deleteButton, replyButton, user);				
			}
		}
		
		return res;
	}
%>

<% 
	@SuppressWarnings("unchecked")
	GrantedAuthority auth = ((List<GrantedAuthority>)(SecurityContextHolder.getContext().getAuthentication().getAuthorities())).get(0);

	out.print(printCommentList(0, javaComments, headers, dateFormat, deleteMessage, indentationMap, auth.toString(), deletionMessage, deleteButton, replyButton, user)); 
%>

<% 
	
	if(request.getParameter("parentId") != null){ //Si el par�metro parentId est� en la url se muestra la caja de comentarios
		out.print("<script>replyFormIn(" + "'comment" + request.getParameter("parentId") + "', " + request.getParameter("rendezvousId") + 
				  ", " + request.getParameter("parentId") + ", '" + indent(indentationMap.get(Integer.valueOf(request.getParameter("parentId")))) + "')</script>");
	}
%>
