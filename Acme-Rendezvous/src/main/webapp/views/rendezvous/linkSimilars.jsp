<%--
 * action-2.jsp
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>

<%@taglib prefix="acme" tagdir="/WEB-INF/tags"%>


<form:form action="rendezvous/user/linkSimilars.do" modelAttribute="rendezvous">

	<form:hidden path="id" />

	<form:label path="links">
		<spring:message code="rendezvous.linkSimilars.links"/>
	</form:label>
	<form:select path="links" multiple="multiple">
		<jstl:forEach items="${rendezvouses}" var="similar">
			<jstl:set var="similarSelected" value="" />
			<jstl:if test="${rendezvous.getLinks().contains(similar)}">
				<jstl:set var="similarSelected" value="selected" />
			</jstl:if>
			<form:option label="${similar.getName()}" value="${similar.getId()}" selected="${similarSelected}" />
		</jstl:forEach>
	</form:select>
	<form:errors path="links" cssClass="error" />

	<br />

	<acme:submit name="linkSimilars" code="rendezvous.linkSimilars.save" />
	<acme:cancel url="rendezvous/display.do?rendezvousId=${rendezvous.getId()}" code="rendezvous.linkSimilars.cancel" />

</form:form>