<%--
 * action-2.jsp
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl"	uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>

<%@taglib prefix="acme" tagdir="/WEB-INF/tags" %>

<form:form method="POST" action="announcement/create.do" modelAttribute="announcement">
	
	<form:hidden path="rendezvous"/>
	<form:hidden path="moment"/>
	
	<acme:textbox code="announcement.create.title" path="title" required="required" />
	
	<br />
	
	<acme:textarea code="announcement.create.description" path="description" required="required" />
	
	<br />
	
	<acme:submit name="save" code="announcement.create.save"/>
	<acme:cancel url="announcement/list.do?rendezvousId=${announcement.getRendezvous().getId()}" code="announcement.create.cancel"/>
	
</form:form>
