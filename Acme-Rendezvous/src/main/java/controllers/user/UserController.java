package controllers.user;

import java.util.Calendar;
import java.util.Collection;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import security.Authority;
import security.LoginService;
import security.UserAccount;
import services.ActorService;
import services.RendezvousService;
import services.UserService;
import domain.Rendezvous;
import domain.User;
import forms.TermsAndConditionsFormObject;

@Controller
@RequestMapping("/user")
public class UserController {
	
	// Services ---------------------------------------------------------------
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private ActorService actorService;
	
	@Autowired
	private RendezvousService rendezvousService;
	
	// Constructor -----------------------------------------------------------

	public UserController() {
		super();
	}
	
	// Methods -----------------------------------------------------------
	
	@RequestMapping(value="/create", method = RequestMethod.GET)
	public ModelAndView create(){
		ModelAndView result;
		TermsAndConditionsFormObject formObject;
		
		formObject = new TermsAndConditionsFormObject();
		result = this.createEditModelAndView(formObject);

		
		return result;
	}
	
	@RequestMapping(value = "/create", method = RequestMethod.POST, params="save")
	public ModelAndView save(TermsAndConditionsFormObject formObject, BindingResult binding){
		ModelAndView result;
		User user;
		
		if(!formObject.getTermsAccepted()){
			result = createEditModelAndView(formObject);
			result.addObject("showMessageTerms", true);
			return result;
		}
		
		user = userService.reconstruct(formObject, binding);
		if(binding.hasErrors()){
			result = createEditModelAndView(formObject);
		} else { 
			try {
				userService.save(user);
				result = new ModelAndView("redirect:/");
			} catch (Throwable oops) {
				result = createEditModelAndView(formObject, "user.commit.error");
			}
		}
		return result;
	}
	
	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public ModelAndView list() {
		ModelAndView result;
		Collection<User> users;

		users = userService.findAll();	
			
		result = new ModelAndView("user/list");
		result.addObject("users", users);
		result.addObject("requestURI", "user/list.do");

		return result;
	}
	
	@RequestMapping(value = "/display", method = RequestMethod.GET)
	public ModelAndView display(@RequestParam(required=false) Integer userId) {
		ModelAndView result;
		User user;
		Collection<Rendezvous> rsvps;
		
		if (userId == null) {
			UserAccount principal;
			
			principal = LoginService.getPrincipal();
			
			user = (User) actorService.findByUserAccountId(principal.getId());
		} else
			user = userService.findOne(userId);
			
		if (SecurityContextHolder.getContext().getAuthentication() != null && 
			SecurityContextHolder.getContext().getAuthentication().isAuthenticated() &&
			!(SecurityContextHolder.getContext().getAuthentication() instanceof AnonymousAuthenticationToken)) {
			UserAccount principal;
			Authority adminAuth;
			
			adminAuth = new Authority();
			adminAuth.setAuthority(Authority.ADMIN);
			
			principal = LoginService.getPrincipal();
			
			if (principal.getAuthorities().contains(adminAuth)) {
				rsvps = rendezvousService.getRSVPSWithoutDraft(user);
			} else {
				User principalUser;
				
				principalUser = (User) actorService.findByUserAccountId(principal.getId());
				
				if (user.equals(principalUser)) {
					rsvps = rendezvousService.getRSVPSAndMyRendezvous(user);
				} else if (isAdult(principalUser)) {
					rsvps = rendezvousService.getRSVPSWithoutDeleted(user);
				} else {
					rsvps = rendezvousService.getRSVPSWithoutDeletedOrAdultsOnly(user);
				}
			}
			
		} else {
			rsvps = rendezvousService.getRSVPSWithoutDeletedOrAdultsOnly(user);
		}
		
		result = new ModelAndView("user/display");
		
		result.addObject("user", user);
		result.addObject("rsvp", rsvps);
		result.addObject("requestURI", "user/display.do");

		return result;
	}
	
	// Ancilliary methods -----------------------------------------------------------
	
	protected ModelAndView createEditModelAndView(TermsAndConditionsFormObject formObject) {
		ModelAndView result; 
		
		result = createEditModelAndView(formObject, null);
		
		return result;
	}

	protected ModelAndView createEditModelAndView(TermsAndConditionsFormObject formObject, String messageCode) {
		ModelAndView result;
		result = new ModelAndView("user/create");
		result.addObject("formObject", formObject);
		result.addObject("message", messageCode);
		
		return result;
	}
	
	private boolean isAdult(User user) {
		Calendar userAdult = Calendar.getInstance();

		userAdult.setTime(user.getBirthday());

		userAdult.set(userAdult.get(Calendar.YEAR) + 18,
				userAdult.get(Calendar.MONTH), userAdult.get(Calendar.DATE));

		return userAdult.getTime().before(new Date());
	}

}
