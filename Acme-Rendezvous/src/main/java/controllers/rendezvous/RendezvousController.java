/*
 * WelcomeController.java
 * 
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the
 * TDG Licence, a copy of which you may download from
 * http://www.tdg-seville.info/License.html
 */

package controllers.rendezvous;

import java.util.Calendar;
import java.util.Collection;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import security.Authority;
import security.LoginService;
import security.UserAccount;
import services.ActorService;
import services.CommentService;
import services.RendezvousService;
import controllers.AbstractController;
import domain.Actor;
import domain.Administrator;
import domain.Rendezvous;
import domain.User;

@Controller
@RequestMapping("/rendezvous")
public class RendezvousController extends AbstractController {

	// Services -----------------------------------------------------------

	@Autowired
	private RendezvousService rendezvousService;

	@Autowired
	private CommentService commentService;
	
	@Autowired
	private ActorService actorService;

	// Constructors -----------------------------------------------------------

	public RendezvousController() {
		super();
	}

	// Index ------------------------------------------------------------------

	@RequestMapping(value = "/display")
	public ModelAndView display(@RequestParam(value = "rendezvousId", required = true) Integer rendezvousId) {
		ModelAndView result;
		Rendezvous rendezvous;
		Collection<? extends GrantedAuthority> principalAuthorities;
		Authority auth;
		
		auth = new Authority();
		auth.setAuthority(Authority.USER);

		rendezvous = rendezvousService.findOne(rendezvousId);
		
		result = new ModelAndView("rendezvous/display");
		
		if((SecurityContextHolder.getContext().getAuthentication() == null || 
				!SecurityContextHolder.getContext().getAuthentication().isAuthenticated() ||
				SecurityContextHolder.getContext().getAuthentication() instanceof AnonymousAuthenticationToken)){
			Assert.isTrue(!rendezvous.isAdultsOnly());
			Assert.isTrue(!rendezvous.isDeleted());
			Assert.isTrue(!rendezvous.isDraft());

		} else {
			UserAccount principal;
			
			principal = LoginService.getPrincipal();
			Assert.isTrue(!rendezvous.isDeleted());
			if (principal.getAuthorities().contains(auth)) {
				User user;
				boolean isAdult;
				
				user = (User) actorService.findPrincipal();
				isAdult = isAdult(user);
				
				result.addObject("user", user);
				result.addObject("isAdult", isAdult);
				
				if (!isAdult)
					Assert.isTrue(!rendezvous.isAdultsOnly());
				
				if (rendezvous.getCreator() != user && !rendezvous.getUsers().contains(user))
					Assert.isTrue(!rendezvous.isDeleted());
			}
		}
		
		Collection<User> users = rendezvous.getUsers();
		
		result.addObject("rendezvous", rendezvous);
		result.addObject("currentDate", new Date());
		result.addObject("comments", commentService.getParentComments(rendezvousId));
		result.addObject("users", users);

		principalAuthorities = SecurityContextHolder.getContext()
				.getAuthentication().getAuthorities();

		if (principalAuthorities.contains(auth)) { // Si se trata de un usuario
			User user;
			
			user = (User) actorService.findPrincipal();
			
			result.addObject("newComment", commentService.create(null, null, null));
			result.addObject("newReply", commentService.create(null, null, null));
			result.addObject("joined", rendezvous.getUsers().contains(user) || rendezvous.getCreator().equals(user));	
		}

		return result;
	}

	// List
	// -------------------------------------------------------------------------
	@RequestMapping("/list")
	public ModelAndView list() {
		// Creamos el objeto a devolver
		ModelAndView result;
		// Creamos una coleccion de rendezvous para almacenar todos
		Collection<Rendezvous> allRendezvous;
		// Cogemos todos los rendezvous del servicio
		
		if(SecurityContextHolder.getContext().getAuthentication() == null || 
			!SecurityContextHolder.getContext().getAuthentication().isAuthenticated() ||
			SecurityContextHolder.getContext().getAuthentication() instanceof AnonymousAuthenticationToken
			|| !isAdult(actorService.findPrincipal())){
			
			allRendezvous = this.rendezvousService.getRendezvousNoDraftNoAdultsOnly();		
		} else {
			allRendezvous = rendezvousService.getRendezvousNoDraft();
		}

		result = new ModelAndView("rendezvous/list");
		// Al modelo y la vista le a�adimos los siguientes atributos
		result.addObject("rendezvous", allRendezvous);
		result.addObject("requestURI", "rendezvous/list.do");

		return result;
	}
	
	private boolean isAdult(Actor user) {
		Calendar userAdult = Calendar.getInstance();

		userAdult.setTime(user.getBirthday());

		userAdult.set(userAdult.get(Calendar.YEAR) + 18,
				userAdult.get(Calendar.MONTH), userAdult.get(Calendar.DATE));

		return userAdult.getTime().before(new Date()) || user instanceof Administrator;
	}
}
