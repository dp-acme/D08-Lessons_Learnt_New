
package controllers.announcement;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import security.LoginService;
import security.UserAccount;
import services.AnnouncementService;
import services.RendezvousService;
import controllers.AbstractController;
import domain.Announcement;
import domain.Rendezvous;

@Controller
@RequestMapping("/announcement")
public class AnnouncementController extends AbstractController {

	// Constructors -----------------------------------------------------------

	public AnnouncementController() {
		super();
	}


	// Services ---------------------------------------------------------------		

	@Autowired
	private AnnouncementService	announcementService;

	@Autowired
	private RendezvousService	rendezvousService;


	// List ---------------------------------------------------------------		

	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public ModelAndView list(@RequestParam(required = false, value = "rendezvousId") Integer rendezvousId) {
		ModelAndView result;

		Collection<Announcement> announcements;
		Rendezvous rendezvous;
		
		rendezvous = null;

		if (rendezvousId != null) {
			rendezvous = rendezvousService.findOne(rendezvousId);
			Assert.notNull(rendezvous);
			
			announcements = this.announcementService.findFromRendezvous(rendezvousId);
		} else {
			UserAccount principal;

			principal = LoginService.getPrincipal();

			announcements = this.announcementService.findFromUserAccountOrderedByDate(principal.getId());
		}

		result = new ModelAndView("announcement/list");
		result.addObject("announcements", announcements);
		result.addObject("rendezvous", rendezvous);

		return result;
	}

	// Create ---------------------------------------------------------------		

	@RequestMapping(value = "/create", method = RequestMethod.GET)
	public ModelAndView create(@RequestParam(required = true, value = "rendezvousId") int rendezvousId) {
		Assert.isTrue(rendezvousId != 0);

		ModelAndView result;
		Announcement announcement;
		UserAccount principal;
		Rendezvous rendezvous;

		principal = LoginService.getPrincipal();
		rendezvous = this.rendezvousService.findOne(rendezvousId);
		Assert.notNull(rendezvous);

		Assert.isTrue(rendezvous.getCreator().getUserAccount().equals(principal));

		announcement = this.announcementService.create(rendezvousId);

		result = this.createEditModelAndView(announcement);

		return result;
	}

	// Save ---------------------------------------------------------------

	@RequestMapping(value = "/create", method = RequestMethod.POST, params = "save")
	public ModelAndView save(Announcement announcement, BindingResult binding) {
		ModelAndView result;
		
		announcement = announcementService.reconstruct(announcement, binding);
		if (binding.hasErrors())
			result = this.createEditModelAndView(announcement);
		else
			try {
				this.announcementService.save(announcement);
				result = new ModelAndView("redirect:list.do?rendezvousId=" + announcement.getRendezvous().getId());
			} catch (Throwable oops) {
				result = this.createEditModelAndView(announcement, "announcement.commit.error");
			}

		return result;
	}

	// Delete ---------------------------------------------------------------		

	@RequestMapping(value = "/delete", method = RequestMethod.GET)
	public ModelAndView delete(@RequestParam(required = true, value = "announcementId") int announcementId) {
		Assert.isTrue(announcementId != 0);

		ModelAndView result;
		Announcement announcement;
		Rendezvous rendezvous;

		announcement = this.announcementService.findOne(announcementId);
		Assert.notNull(announcement);

		rendezvous = announcement.getRendezvous();

		try {
			this.announcementService.delete(announcement);
			result = new ModelAndView("redirect:list.do?rendezvousId=" + rendezvous.getId());
		} catch (Throwable oops) {
			result = new ModelAndView("redirect:list.do?rendezvousId=" + rendezvous.getId());
			result.addObject("message", "announcement.commit.error");
		}

		return result;
	}

	// Ancillary methods ---------------------------------------------------------------		

	protected ModelAndView createEditModelAndView(Announcement announcement) {
		ModelAndView result;

		result = this.createEditModelAndView(announcement, null);

		return result;
	}

	protected ModelAndView createEditModelAndView(Announcement announcement, String messageCode) {
		ModelAndView result;

		result = new ModelAndView("announcement/edit");
		result.addObject("announcement", announcement);

		result.addObject("message", messageCode);

		return result;
	}

}
