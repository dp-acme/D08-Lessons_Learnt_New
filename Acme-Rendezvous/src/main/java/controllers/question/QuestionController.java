
package controllers.question;

import java.util.Calendar;
import java.util.Collection;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import security.LoginService;
import security.UserAccount;
import services.ActorService;
import services.QuestionService;
import services.RendezvousService;
import controllers.AbstractController;
import domain.Actor;
import domain.Administrator;
import domain.Question;
import domain.Rendezvous;
import domain.User;

@Controller
@RequestMapping("/question")
public class QuestionController extends AbstractController {

	@Autowired
	private QuestionService		questionService;

	@Autowired
	private RendezvousService	rendezvousService;

	@Autowired
	private ActorService		actorService;


	public QuestionController() {
		super();
	}

	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public ModelAndView list(@RequestParam("rendezvousId") int rendezvousId) {
		ModelAndView result;
		Collection<Question> questions;
		Boolean creator = null;
		Rendezvous rendezvous;

		rendezvous = this.rendezvousService.findOne(rendezvousId);
		Assert.notNull(rendezvous);

		questions = rendezvous.getQuestions();
		
		Assert.isTrue(!rendezvous.isDeleted());

		if(!(SecurityContextHolder.getContext().getAuthentication() == null || 
			!SecurityContextHolder.getContext().getAuthentication().isAuthenticated() ||
			SecurityContextHolder.getContext().getAuthentication() instanceof AnonymousAuthenticationToken)) {
			UserAccount principal;
			
			principal = LoginService.getPrincipal();
			
			if (rendezvous.isDraft()) {
				Assert.isTrue(rendezvous.getCreator().getUserAccount().equals(principal));
			}
			
			Actor actor;
			actor = actorService.findPrincipal();
			
			Assert.isTrue(!rendezvous.isAdultsOnly() || 
				(actor instanceof User && isAdult(actor) && rendezvous.isAdultsOnly()) || 
				actor instanceof Administrator);
		} else {
			Assert.isTrue(!rendezvous.isAdultsOnly());
			Assert.isTrue(!rendezvous.isDraft());
		}

		try {
			UserAccount principal = LoginService.getPrincipal();
			if (principal != null) {
				User user = (User) this.actorService.findByUserAccountId(principal.getId());
				if (rendezvous.getCreator().equals(user))
					creator = true;
				else {
//					Assert.isTrue(rendezvous.getMoment().after(new Date()));
					Assert.isTrue(!rendezvous.isDraft());
				}
			}
		} catch (Exception e) {
			creator = false;
//			Assert.isTrue(rendezvous.getMoment().after(new Date()));
			Assert.isTrue(!rendezvous.isDraft());
		}

		result = new ModelAndView("question/list");
		result.addObject("questions", questions);
		result.addObject("creator", creator);
		result.addObject("rendezvous", rendezvous);

		return result;
	}

	@RequestMapping(value = "/user/create", method = RequestMethod.GET)
	public ModelAndView create(@RequestParam("rendezvousId") int rendezvousId) {
		Assert.isTrue(rendezvousId != 0);
		
		ModelAndView result;
		Question question;

		question = this.questionService.create(rendezvousId);
		
		result = createEditModelAndView(question);

		return result;
	}

	@RequestMapping("/user/edit")
	public ModelAndView edit(@RequestParam int questionId) {
		ModelAndView result;
		Question question;

		question = this.questionService.findOne(questionId);

		Assert.notNull(question);

		result = this.createEditModelAndView(question);

		return result;
	}

	@RequestMapping("/user/delete")
	public ModelAndView delete(@RequestParam int questionId) {
		ModelAndView result;

		Question question = this.questionService.findOne(questionId);

		try {
			this.questionService.delete(question);

			result = new ModelAndView("redirect:/question/list.do?rendezvousId=" + question.getRendezvous().getId());

		} catch (final Throwable oops) {
			result = this.createEditModelAndView(question, "question.commit.error");
		}

		return result;
	}

	@RequestMapping(value = "/user/edit", method = RequestMethod.POST, params = "save")
	public ModelAndView save(Question question, BindingResult binding) {
		ModelAndView result;
		Rendezvous rendezvous;
		Question questionRes;
		
		questionRes = questionService.reconstruct(question, binding);

		if (binding.hasErrors())
			result = this.createEditModelAndView(questionRes);
		else
			try {
				rendezvous = questionRes.getRendezvous();
				this.questionService.save(questionRes);
				result = new ModelAndView("redirect:/question/list.do?rendezvousId=" + rendezvous.getId());
			} catch (Throwable oops) {
				result = this.createEditModelAndView(question, oops.getMessage());
			}

		return result;
	}

	protected ModelAndView createEditModelAndView(Question question) {
		ModelAndView result;

		result = this.createEditModelAndView(question, null);

		return result;
	}

	protected ModelAndView createEditModelAndView(Question question, String messageCode) {
		ModelAndView result;
		Rendezvous rendezvous;
		
		rendezvous = question.getRendezvous();
		Assert.notNull(rendezvous);

		Assert.isTrue(rendezvous.isDraft());
		Assert.isTrue(!rendezvous.isDeleted());
		Assert.isTrue(rendezvous.getMoment().after(new Date()));
		
		User user = (User) this.actorService.findPrincipal();
		Assert.isTrue(rendezvous.getCreator().equals(user));

		result = new ModelAndView("question/user/edit");
		result.addObject("question", question);
		result.addObject("rendezvous", rendezvous);
		result.addObject("message", messageCode);

		return result;
	}
	
	private boolean isAdult(Actor user) {
		Calendar userAdult = Calendar.getInstance();

		userAdult.setTime(user.getBirthday());

		userAdult.set(userAdult.get(Calendar.YEAR) + 18,
				userAdult.get(Calendar.MONTH), userAdult.get(Calendar.DATE));

		return userAdult.getTime().before(new Date()) || user instanceof Administrator;
	}
}
