/*
 * WelcomeController.java
 * 
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the
 * TDG Licence, a copy of which you may download from
 * http://www.tdg-seville.info/License.html
 */

package controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/welcome")
public class WelcomeController extends AbstractController {

	// Constructors -----------------------------------------------------------

	public WelcomeController() {
		super();
	}

	// Index ------------------------------------------------------------------		
	
	@RequestMapping(value = "/index")
	public ModelAndView index() {
		ModelAndView result;

		result = new ModelAndView("redirect:rendezvous/list.do");

		return result;
	}
	
	@RequestMapping(value = "/aboutUs")
	public ModelAndView aboutUs() {
		ModelAndView result;

		result = new ModelAndView("welcome/aboutUs");

		return result;
	}
	
	@RequestMapping(value = "faq")
	public ModelAndView faq() {
		ModelAndView result;

		result = new ModelAndView("welcome/faq");

		return result;
	}
	
	@RequestMapping(value = "termsAndConditions")
	public ModelAndView termsAndConditions() {
		ModelAndView result;

		result = new ModelAndView("welcome/termsAndConditions");

		return result;
	}
}
