package converters;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import repositories.UserRepository;
import domain.User;

@Component
@Transactional
public class StringToUserConverter implements Converter<String, User>{

	@Autowired UserRepository userRepository;
	
	@Override
	public User convert(String source) {
		User result;
		int id;
		
		try{
			if(StringUtils.isEmpty(source))
				result = null;
			else{
				id = Integer.valueOf(source);
				result = userRepository.findOne(id);
			}
		}catch(Throwable oops){
			throw new IllegalArgumentException(oops);
		}
		return result;
	}

}
