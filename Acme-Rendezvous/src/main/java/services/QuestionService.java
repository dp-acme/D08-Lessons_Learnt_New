package services;

import java.util.ArrayList;
import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;

import repositories.QuestionRepository;
import security.LoginService;
import security.UserAccount;
import domain.Answer;
import domain.Question;
import domain.Rendezvous;

@Service
@Transactional
public class QuestionService {
	
	@Autowired
	private QuestionRepository questionRepository;

	@Autowired
	private RendezvousService rendezvousService;
	
	@Autowired
	private Validator validator;
	
	public QuestionService(){
		super();
	}
	
	public Question create(int rendezvousId){
		Assert.isTrue(rendezvousId != 0);
		
		Question result;
		Rendezvous rendezvous;
		
		rendezvous = rendezvousService.findOne(rendezvousId);
		Assert.notNull(rendezvous);
		
		result = new Question();
		
		result.setAnswers(new ArrayList<Answer>());
		result.setRendezvous(rendezvous);
		
		return result;	
	}
	
	public Question findOne(int questionId){
		Assert.isTrue(questionId != 0);
		
		Question result;
		
		result = questionRepository.findOne(questionId);
		
		return result;
	}
	
	public Collection<Question> findAll(){
		Collection<Question> result;
		
		result = questionRepository.findAll();
		Assert.notNull(result);
		
		return result;
	}
	
	public void delete(Question question){
		Assert.notNull(question);	
		
		Rendezvous rendezvous;
		UserAccount principal;
		
		principal = LoginService.getPrincipal();
		Assert.isTrue(question.getRendezvous().getCreator().getUserAccount().equals(principal));
		rendezvous = rendezvousService.findOne(question.getRendezvous().getId());
		
		rendezvous.getQuestions().remove(question);
		
		rendezvousService.save(rendezvous);
		questionRepository.delete(question);
	}
	
	public Question save(Question question){
		Assert.notNull(question);
		
		Question result;
		Rendezvous rendezvous;
		UserAccount principal;
		
		principal = LoginService.getPrincipal();
		Assert.isTrue(question.getRendezvous().getCreator().getUserAccount().equals(principal));
		rendezvous = rendezvousService.findOne(question.getRendezvous().getId());
				
		result = questionRepository.save(question);
		
		if(question.getId() == 0){
			rendezvous.getQuestions().add(result);
			rendezvousService.save(rendezvous);
		}
		
		rendezvous.getQuestions().add(result);
		rendezvousService.save(rendezvous);
		
		return result;
	}
	
	public void deleteAsnwer(Answer answer){
		Assert.notNull(answer);
		
		UserAccount principal;
		Question question;
		
		principal = LoginService.getPrincipal();
		Assert.isTrue(answer.getUser().getUserAccount().equals(principal));
		
		question = answer.getQuestion();
		question.getAnswers().remove(answer);
		questionRepository.save(question);
	}
	
	public void addAnswer(Answer answer){
		Assert.notNull(answer);
		
		UserAccount principal;
		Question question;
		
		principal = LoginService.getPrincipal();
		Assert.isTrue(answer.getUser().getUserAccount().equals(principal));
		
		question = answer.getQuestion();
		question.getAnswers().add(answer);
		
		questionRepository.save(question);
	}
	
	public Question reconstruct(Question question,  BindingResult binding){
		Question result;
		
		
		if(question.getId() == 0){
			result = create(question.getRendezvous().getId());
		}else{
			result = questionRepository.findOne(question.getId());	
		}
		
		result.setText(question.getText());
		
		validator.validate(result, binding);
		
		return result;
	}
	
	

}
